# similarity_phish_detect

## Summary 
This tool analyzes folders full of HTML files for specific tags (chosen by user), calculates distance matrices (based on distance matrix chosen by user), performs clustering and finds best threshold value at which to stop clustering. It also allows for visualizing results through a line-graph. It is set up for graphing precomputed clustering results stored in an SQLite database. It produces a logging file when both analyzing and when graphing at the specified output_directory. 

## Using Data Clustering and Graphing Tools
To run similarity_phish_detect, you will need to download the repostiory and then cd into its code folder. It is reccomended you launch a virtual environment so as not to run into any issues regareding versions of pakcages. Once this is done, you can run 
`pip install -r requirements.txt` to download all necessary packages in your virtual environment. Once this is done, you can fun the tool via the command line with two main actions:

Analyze: Perform clustering analysis on HTML files.
Graph: Generate graphs from clustering results stored in SQLite.

To analyze, you must run 
`python main.py analyze <directory_with_html_files_to_analyze> <output_directory> [options]`
where 
- directory_with_html_files_to_analyze is the directory containing HTML files for analysis
- output_directory is where results (meaning a sqlite file as well as a logging file) will be saved
- [options] are all optional, with a default value if not specified. These include: 
1. --tags: List of tags to count in HTML files. You can put any valid html tag here. Seperate tags with a space and they will be treated as a list. For example:
 `python main.py analyze <directory_with_html_files_to_analyze> <output_directory> --tags p h1 header h2]` will have analysis run on tag vectors consisting of just counts of how many times the tags p, h1, header, and h2 show up in the files in the directory_with_html_files_to_analyze folder. 
 2. --sample_distance: Distance metric for clustering (options here are wpd or pd). Defaults to wpd.
 3. --linkages: Linkage methods for clustering (complete, average, single). Defaults to calculating all three. Each linkage method will get its own sqlite datafile for ease of usage when graphing. You can choose just one, or any combinations of them, so long as linkage methods are seperating by a space. For example, 
`python main.py analyze <directory_with_html_files_to_analyze> <output_directory> --linkages complete average]` will have analysis run using both complete and average linkage, with one sqlite datafile being created in output directory for results gotten from complete and one for results gotten from average. 
4. --qoc_function: quality of clustering function to be used in analysis (default, weighted, normalized_weighted). Default is default. 

To graph, you must run
python main.py graph <sqlite_file_including_path> <table_name> <output_directory> [options]
where 
- <sqlite_file_including_path> is the path to, including the file itself of the sqlite file you want to see information graphed from. 
- <table_name> is the name of the specific table from the sqlite_file_including_path sqlite file that you want to graph information from. If you are graphing information output from runnning `analyze`, the <table_name> will be clustering_results. 
- <output_directory> is the directory in which you want to save the generated graph. 
- [options] are all optional, with a default value if not specified. These include: 
1. --x_column: Name of column which will be graphed on x-axis of our graph. Defaults to 'threshold'
2. --y-column: Name of column which will be graphed on y-axis of our graph. Defaults to 'qoc'
3. --x_limits : Specification of the range of values to display on the x-axis of the graph. Ex. "0,.25". Defaults to None. 
4. --y_limits : Specification of the range of values to display on the y-axis of the graph. Ex. "0,.25". Defaults to None. 


## similarity_phish_detect authors and acknowledgement 
This is the code for a senior thesis project being worked on by Aleksandra Jacewicz under Norman Danner as a part of the Programming Languages and Privacy research group at Wesleyan University.

The code being hosted here is inspired by the [research](https://ruor.uottawa.ca/handle/10393/39891) done by Qian Cui as part of his PhD thesis at the University of Ottawa under Guy-Vincent Jourdan. We are interested in the procedure refered to as detecting phishing replicas and variations in chapter 3 of the thesis. We are specifically interested in extending the clustering done so as to create a classifier that determines if a website is a phishing site, at zero-hour launch. 


## Roadmap
I am currently working on finishing up refactoring the code I have written up until this point (which processes a folder full of HTML files based on forming HTML tag vectors and then finds the best threshold for cutting off agglomerative clustering based on a measure of quality of clustering) so as to allow specifications in the termianal of various relevant parameters. The immediate next step is building a classifier as based on the results of agglomerative clustering based on a good threshold and evaluating its accuracy. 

## Python Package Dependencies 
These are all listed in requirements.txt, which can be found in the code folder of this repository. 
