from bs4 import BeautifulSoup
from collections import Counter
from typing import List, Dict, Tuple
import os
import doctest 
import doctest 
from multiprocessing import Pool
import concurrent.futures 
import seaborn as sns
import logging
from system_usage import log_system_usage
from HtmlTagNames import html_tag_names



# def process_html_files(folder_path: str, tags_of_interest: List[str]) -> Dict[str, Dict[str, int]]:
#     """
#     Processes HTML files in a folder and counts occurrences of specified tags.

#     Parameters:
#     - folder_path (str): The path to the folder containing HTML files.
#     - tags_of_interest (List[str]): A list of tag names to count occurrences of in the HTML content.

#     Returns:
#     - Dict[str, Dict[str, int]]: A dictionary where keys are file names and values are dictionaries where keys are tag names from tags_of_interest and values are counts of their occurrences.
#     Examples:
#         >>> process_html_files('tests/test1', ['header', 'h1', 'section', 'h2', 'p', 'h3', 'li'])
#         {'file1.html': {'header': 1, 'h1': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}, 'file2.html': {'header': 0, 'h1': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0, 'li': 2}}

#         >>> process_html_files('tests/test1', ['header', 'section', 'h2', 'p', 'h3', 'li'])
#         {'file1.html': {'header': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}, 'file2.html': {'header': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0, 'li': 2}}

#         >>> process_html_files('tests/test1', ['li', 'header', 'section', 'h2', 'p', 'h3'])
#         {'file1.html': {'li': 6, 'header': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2}, 'file2.html': {'li': 2, 'header': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0}}

#         >>> process_html_files('tests/test2', ['li', 'header', 'section', 'h2', 'p', 'h3'])
#         {'file3.html': {'li': 0, 'header': 0, 'section': 0, 'h2': 0, 'p': 0, 'h3': 0}, 'file4.html': {'li': 0, 'header': 0, 'section': 0, 'h2': 0, 'p': 1, 'h3': 0}}
#     """
#     start_time = datetime.now()
#     # Initialize an empty dictionary to store tag counts for each file
#     file_tag_counts = {}

#     # Iterate over each file in the folder
#     for filename in os.listdir(folder_path):
#         if filename.endswith('.html'):
#             file_path = os.path.join(folder_path, filename)
#             with open(file_path, 'r', encoding='utf-8') as file:
#                 html_content = file.read()
#                 soup = BeautifulSoup(html_content, 'html.parser')
#                 tags = [tag.name for tag in soup.find_all()]
#                 tag_counts = Counter(tags)

#                 # Initialize dictionary for current file
#                 tag_counts_all = {tag: tag_counts[tag] if tag in tag_counts else 0 for tag in tags_of_interest}

#                 # Update tag counts for current file
#                 file_tag_counts[filename] = tag_counts_all
#     end_time = datetime.now() 
#     execution_time = end_time - start_time
#     print(f"process HTML file execution time: {execution_time}")
#     return file_tag_counts


def process_single_file(filename : str, html_content: str, tags_of_interest: List[str]) -> Dict[str, Dict[str, int]]:
    """
    Process a single HTML file and count occurrences of specified tags.
    
    Parameters:
    - filename (str): Name of the file being processed.
    - html_content (str): Content of the HTML file.
    - tags_of_interest (List[str]): List of tags to count.
    
    Returns:
    - Dict[str, Dict[str, int]]: A dictionary where the key is the filename and the value is another dictionary 
                                   with tag counts.
    """
    # folder_path, filename, tags_of_interest = args
    # file_path = os.path.join(folder_path, filename)
    # tag_counts_all = {}
    
    # if os.path.isfile(file_path):
    #     with open(file_path, 'r', encoding='utf-8') as file:
    #         html_content = file.read()
            # print(f"Processing file: {filename}")
    # print(f"our html content looks like {html_content}")
    soup = BeautifulSoup(html_content, 'html.parser')
    tags = [tag.name for tag in soup.find_all()]
    tag_counts = Counter(tags)

    tag_counts_all = {tag: tag_counts[tag] if tag in tag_counts else 0 for tag in tags_of_interest}
    # else:
    #     print(f"File {file_path} does not exist.")

    return {filename: tag_counts_all}

def process_html_files_p(html_files: Dict[str, str], tags_of_interest: List[str]) -> Dict[str, Dict[str, int]]:
    """
    Processes HTML files in a folder and counts occurrences of specified tags.
    
    Parameters:
    - html_files (Dict[str, str]): Dictionary of filenames and their HTML contents.
    - tags_of_interest (List[str]): A list of tag names to count occurrences of in the HTML content.
    
    Returns:
    - Dict[str, Dict[str, int]]: A dictionary where keys are file names and values are dictionaries
    where keys are tag names from tags_of_interest and values are counts of their occurrences.
    >>> html_files0 = {
    ...     "file1.html": "<html><body><div>Hello</div><div>World</div></body></html>",
    ...     "file2.html": "<html><body><p>Paragraph</p><p>Another</p></body></html>"
    ... }
    >>> tags_of_interest0 = ["div", "p"]
    >>> process_html_files_p(html_files0, tags_of_interest0)  # doctest: +SKIP
    {
        "file1.html": {"div": 2, "p": 0},
        "file2.html": {"div": 0, "p": 2}
    }
    >>> tags_of_interest1 = ["p", "div"]
    >>> process_html_files_p(html_files0, tags_of_interest1)  # doctest: +SKIP
    {
        "file1.html": {"div": 2, "p": 0},
        "file2.html": {"div": 0, "p": 2}
    }

    >>> html_files1 = {
    ...     "file1.html": '<!DOCTYPE html> <html lang="en"> <head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Website 1</title> </head> <body> <header> <h1>Website 1</h1> </header> <main> <section> <h2>Section 1</h2> <p>This is a paragraph in section 1.</p> <p>Another paragraph in section 1.</p> <div> <h3>Subsection 1</h3> <p>This is a paragraph in subsection 1.</p> </div> </section> <section> <h2>Section 2</h2> <ul> <li>List item 1</li> <li>List item 2</li> <li>List item 3</li> </ul> <div> <h3>Subsection 2</h3> <ol> <li>Ordered item 1</li> <li>Ordered item 2</li> <li>Ordered item 3</li> </ol> </div> </section> </main> <footer> <p>&copy; Website 1</p> </footer> </body> </html>',
    ...     "file2.html": '<!DOCTYPE html> <html lang="en"> <head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Example 1</title> </head> <body> <section> <h2>Main Section</h2> <p>This is a paragraph inside the main section.</p> <ul> <li>List item 1</li> <li>List item 2</li> </ul> </section> </body> </html>',
    ...     "file3.html": '',
    ...     "file4.html": '<!DOCTYPE html> <html lang="en"> <head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Sample HTML</title> </head> <body> <p>This is a paragraph of text.</p> </body> </html>',
    ...     "cfile1.html": '<!DOCTYPE html> <html lang="en"> <head> <meta charset="UTF-8"> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Website 1</title> </head> <body> <header> <h1>Website 1</h1> </header> <main> <section> <h2>Section 1</h2> <p>This is a paragraph in section 1.</p> <p>Another paragraph in section 1.</p> <div> <h3>Subsection 1</h3> <p>This is a paragraph in subsection 1.</p> </div> </section> <section> <h2>Section 2</h2> <ul> <li>List item 1</li> <li>List item 2</li> <li>List item 3</li> </ul> <div> <h3>Subsection 2</h3> <ol> <li>Ordered item 1</li> <li>Ordered item 2</li> <li>Ordered item 3</li> </ol> </div> </section> </main> <footer> <p>&copy; Website 1</p> </footer> </body> </html>'
    ... }

    >>> process_html_files_p(html_files1, ['header', 'h1', 'section', 'h2', 'p', 'h3', 'li'])
    {'file1.html': {'header': 1, 'h1': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}, 'file2.html': {'header': 0, 'h1': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0, 'li': 2}, 'file3.html': {'header': 0, 'h1': 0, 'section': 0, 'h2': 0, 'p': 0, 'h3': 0, 'li': 0}, 'file4.html': {'header': 0, 'h1': 0, 'section': 0, 'h2': 0, 'p': 1, 'h3': 0, 'li': 0}, 'cfile1.html': {'header': 1, 'h1': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}}

    >>> process_html_files_p(html_files1, ['header', 'section', 'h2', 'p', 'h3', 'li'])  # doctest: 
    {'file1.html': {'header': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}, 'file2.html': {'header': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0, 'li': 2}, 'file3.html': {'header': 0, 'section': 0, 'h2': 0, 'p': 0, 'h3': 0, 'li': 0}, 'file4.html': {'header': 0, 'section': 0, 'h2': 0, 'p': 1, 'h3': 0, 'li': 0}, 'cfile1.html': {'header': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}}

    """
    file_tag_counts = {}
    # files = [f for f in os.listdir(folder_path) if f.endswith('.html')]
    
    with Pool() as pool:
        args = [(filename, content, tags_of_interest) for filename, content in html_files.items()]
        results = pool.starmap(process_single_file, args) 

    for result in results:
        file_tag_counts.update(result)

    # print("File tag counts are:")
    # print(file_tag_counts)
    return file_tag_counts

# def transform_tag_counts_to_lists(file_tag_counts: Dict[str, Dict[str, int]]) -> Dict[str, List[int]]:
#     """
#     Examples:
#     >>> transform_tag_counts_to_lists({'file1': {'tag1': 10, 'tag2': 20}, 'file2': {'tag1': 15, 'tag2': 25}})
#     {'file1': [10, 20], 'file2': [15, 25]}
#     >>> transform_tag_counts_to_lists({'file1': {'tag1': 5, 'tag2': 10, 'tag3': 15}})
#     {'file1': [5, 10, 15]}
#     >>> transform_tag_counts_to_lists({'file1': {'tag1': 0, 'tag2': 0}})
#     {'file1': [0, 0]}

#     # these test to make sure that the list returned still has a consistent order even if tags
#     # input are originally not all input in the same order (this may happen given that
#     # we are using dictionaries, which are not inherently ordered)
#     >>> transform_tag_counts_to_lists({'file1': {'tag2': 20, 'tag1': 10}, 'file2': {'tag1': 15, 'tag2': 25}})
#     {'file1': [10, 20], 'file2': [15, 25]}
#     >>> transform_tag_counts_to_lists({'file1': {'tag2': 20, 'tag1': 10}, 'file2': {'tag2': 25, 'tag1': 15}})
#     {'file1': [10, 20], 'file2': [15, 25]}
#     >>> transform_tag_counts_to_lists({'file1': {'tag2': 20, 'tag1': 10, 'grays': 6, 'zebra': 100}, 'file2': {'grays': 6, 'zebra': 100, 'tag2': 20, 'tag1': 10}})
#     {'file1': [6, 10, 20, 100], 'file2': [6, 10, 20, 100]}

#     Transforms tag counts from dictionary format to a dictionary of lists format.

#     Parameters:
#     - file_tag_counts (Dict[str, Dict[str, int]]): A dictionary where keys are file names and values are dictionaries containing tag counts.

#     Returns:
#     - Dict[str, List[int]]: A dictionary where keys are file names and values are lists of integers representing tag counts.
#     """
#     transformed_counts: Dict[str, List[int]] = {}

#     for filename, tag_counts in file_tag_counts.items():
#         tags = sorted(tag_counts.keys())
#         transformed_counts[filename] = [tag_counts[tag] for tag in tags]

#     return transformed_counts

def process_file_tag_counts(item: Tuple[str, Dict[str, int]]) -> Tuple[str, List[int]]:
    """
    Helper function to transform tag counts for a single file.

    Parameters:
    - item (Tuple[str, Dict[str, int]]): A tuple containing the filename and its corresponding tag counts.

    Returns:
    - Tuple[str, List[int]]: A tuple containing the filename and the corresponding list of tag counts.
    >>> process_file_tag_counts(('file1', {'tag1': 10, 'tag2': 20}))
    ('file1', [10, 20])
    
    >>> process_file_tag_counts(('file2', {'tag3': 30, 'tag1': 15, 'tag2': 25}))
    ('file2', [15, 25, 30])
    
    >>> process_file_tag_counts(('file3', {'tag1': 5, 'tag2': 5, 'tag3': 10}))
    ('file3', [5, 5, 10])
    """
    filename, tag_counts = item
    tags = sorted(tag_counts.keys())
    counts = [tag_counts[tag] for tag in tags]

    return filename, counts

def transform_tag_counts_to_lists_p(file_tag_counts: Dict[str, Dict[str, int]]) -> Dict[str, List[int]]:
    """
    Transforms tag counts from dictionary format to a dictionary of lists format. Filters out any vector that contains
    only 0s for all tag counts. 

    Parameters:
    - file_tag_counts (Dict[str, Dict[str, int]]): A dictionary where keys are file names and values are dictionaries containing tag counts.

    Returns:
    - Dict[str, List[int]]: A dictionary where keys are file names and values are lists of integers representing tag counts.
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 10, 'tag2': 20}, 'file2': {'tag1': 15, 'tag2': 25}})
    {'file1': [10, 20], 'file2': [15, 25]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 5, 'tag2': 10, 'tag3': 15}})
    {'file1': [5, 10, 15]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 0, 'tag2': 0}})
    {}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag2': 20, 'tag1': 10}, 'file2': {'tag1': 15, 'tag2': 25}})
    {'file1': [10, 20], 'file2': [15, 25]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag2': 20, 'tag1': 10}, 'file2': {'tag2': 25, 'tag1': 15}})
    {'file1': [10, 20], 'file2': [15, 25]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag2': 20, 'tag1': 10, 'grays': 6, 'zebra': 100}, 'file2': {'grays': 6, 'zebra': 100, 'tag2': 20, 'tag1': 10}})
    {'file1': [6, 10, 20, 100], 'file2': [6, 10, 20, 100]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 0, 'tag2': 0}, 'file2': {'tag1': 15, 'tag2': 25}})
    {'file2': [15, 25]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 0, 'tag2': 0}, 'file2': {'tag1': 15, 'tag2': 25}, 'file3' : {'tag1': 0, 'tag2': 0}, 'file4' : {'tag1': 0, 'tag2': 0}})
    {'file2': [15, 25]}
    """
    with concurrent.futures.ProcessPoolExecutor() as executor:
        results = executor.map(process_file_tag_counts, file_tag_counts.items())

    transformed_counts = dict(results)
    # getting rid of all tag vectors full of all 0s because they cause an issue with wpd 
    # inherent to wpd definition 
    filtered_counts = {filename: counts for filename, counts in transformed_counts.items() if not all(count == 0 for count in counts)}
    
    # print("Filtered counts are:")
    # print(filtered_counts)
    # print(f"filtered counts look like: {filtered_counts}")
    return filtered_counts

def map_lists_to_strings(tag_counts: Dict[str, List[int]]) -> Tuple[Dict[tuple, List[str]], List[tuple]]:
    """
    Processes a dictionary of file name to tag vector mapping to output a reverse mapping where unique tag vectors
    are mapped to file names, which are grouped together and to a list of unique tag count vectors 
    >>> map_lists_to_strings({'file1': [1,2,3], 'file2': [1,2,4]})
    ({(1, 2, 3): ['file1'], (1, 2, 4): ['file2']}, [(1, 2, 3), (1, 2, 4)])
    >>> map_lists_to_strings({'file1': [1,2,3], 'file2': [1,2,3]})
    ({(1, 2, 3): ['file1', 'file2']}, [(1, 2, 3)])
    >>> map_lists_to_strings({'file2': [1,2,3], 'file1': [1,2,3]})
    ({(1, 2, 3): ['file2', 'file1']}, [(1, 2, 3)])
    >>> map_lists_to_strings({})
    ({}, [])
    >>> map_lists_to_strings({'file1': [1,2,3], 'file2': [1,2,4], 'file3': [1,2,3], 'file4' : [1,2,4]})
    ({(1, 2, 3): ['file1', 'file3'], (1, 2, 4): ['file2', 'file4']}, [(1, 2, 3), (1, 2, 4)])
    """
    reversed_mapping = {}
    unique_counts = set()
    for filename, counts in tag_counts.items():
        counts_tuple = tuple(counts) 
        unique_counts.add(counts_tuple)
        if counts_tuple in reversed_mapping:
            reversed_mapping[counts_tuple].append(filename)
        else:
            reversed_mapping[counts_tuple] = [filename]
    return reversed_mapping, sorted(unique_counts)

def all_together(html_files: Dict[str, str], tags_of_interest: List[str]) -> List[tuple]:
    logging.info("starting to process HTML files and count tags")
    tag_counts = process_html_files_p(html_files, tags_of_interest)
    logging.info("we've found tag counts")
    log_system_usage("After finding tag counts")
    tlist = transform_tag_counts_to_lists_p(tag_counts)
    logging.info("we've transformed tag counts to lists")
    log_system_usage("After transforming tag counts")
    reverse_map, tag_vecs = map_lists_to_strings(tlist)
    logging.info("we've mapped lists to strings")
    log_system_usage("After mapping lists to strings")
    ntag = len(tag_vecs)
    # print(f"the number of unique tag vectors is {ntag}")
    return tag_vecs


def load_image_clusters(image_cluster_folder : str) -> Dict[str, set]:
    """ Reads manually created cluster based on images when given the name of the directly where all subdirectories
    are located. Will return a dictionary mapping cluster_labels to set of filenames of all images that are within that cluster
    """
    image_clusters = {}
    for cluster_label in os.listdir(image_cluster_folder):
        cluster_path = os.path.join(image_cluster_folder, cluster_label)
        if os.path.isdir(cluster_path):
            image_clusters[cluster_label] = set(os.listdir(cluster_path))
    return image_clusters

# def get_html_from_images(file_name, db_path):
#     """
#     Need to figure out how to take picture names in files and find the HTML that corresponds to them
#     Don't think this can be done based on info found in random1000, need to access database?
#     Talk to Danner about this 

#     Queries database to find the corresponding HTML files for a given image filename. 
#     Maybe return a dictionary mapping image_filename : corresponding_html_file
#     """

def convert_image_clusters_to_html_clusters(image_clusters, db_path):
    """ return dictionary mapping cluster label : corresponding HTML files in it
    """
    html_clusters = {}
    for cluster_label, image_filenames in image_clusters.items():
        html_files = get_html_from_images(image_filenames, db_path)
        html_clusters[cluster_label] = set(html_files.values())
    return html_clusters