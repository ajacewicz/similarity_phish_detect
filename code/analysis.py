
# Analysis (module 2) - using the HTML collected in module 1 to form the tag vectors needed for further
# clustering; also going to have remainder of clustering code. 

from bs4 import BeautifulSoup
from collections import Counter
from typing import List, Dict, Tuple, Callable 
import os
import doctest 
from sklearn.cluster import AgglomerativeClustering
from itertools import combinations
import doctest 
import matplotlib.pyplot as plt
import numpy as np
from multiprocessing import Pool
import concurrent.futures 
from datetime import datetime
from itertools import combinations
import random
import seaborn as sns
import logging 
import psutil



def log_system_usage(step: str) -> None:
    process = psutil.Process(os.getpid())
    memory = process.memory_info().rss / (1024 * 1024)  # Convert to MB
    cpu = process.cpu_percent(interval=0.1)  # Get CPU usage over a short interval
    logging.info(f"[{step}] Memory Usage: {memory:.2f} MB, CPU Usage: {cpu:.2f}%")


# def process_html_files(folder_path: str, tags_of_interest: List[str]) -> Dict[str, Dict[str, int]]:
#     """
#     Processes HTML files in a folder and counts occurrences of specified tags.

#     Parameters:
#     - folder_path (str): The path to the folder containing HTML files.
#     - tags_of_interest (List[str]): A list of tag names to count occurrences of in the HTML content.

#     Returns:
#     - Dict[str, Dict[str, int]]: A dictionary where keys are file names and values are dictionaries where keys are tag names from tags_of_interest and values are counts of their occurrences.
#     Examples:
#         >>> process_html_files('tests/test1', ['header', 'h1', 'section', 'h2', 'p', 'h3', 'li'])
#         {'file1.html': {'header': 1, 'h1': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}, 'file2.html': {'header': 0, 'h1': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0, 'li': 2}}

#         >>> process_html_files('tests/test1', ['header', 'section', 'h2', 'p', 'h3', 'li'])
#         {'file1.html': {'header': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}, 'file2.html': {'header': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0, 'li': 2}}

#         >>> process_html_files('tests/test1', ['li', 'header', 'section', 'h2', 'p', 'h3'])
#         {'file1.html': {'li': 6, 'header': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2}, 'file2.html': {'li': 2, 'header': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0}}

#         >>> process_html_files('tests/test2', ['li', 'header', 'section', 'h2', 'p', 'h3'])
#         {'file3.html': {'li': 0, 'header': 0, 'section': 0, 'h2': 0, 'p': 0, 'h3': 0}, 'file4.html': {'li': 0, 'header': 0, 'section': 0, 'h2': 0, 'p': 1, 'h3': 0}}
#     """
#     start_time = datetime.now()
#     # Initialize an empty dictionary to store tag counts for each file
#     file_tag_counts = {}

#     # Iterate over each file in the folder
#     for filename in os.listdir(folder_path):
#         if filename.endswith('.html'):
#             file_path = os.path.join(folder_path, filename)
#             with open(file_path, 'r', encoding='utf-8') as file:
#                 html_content = file.read()
#                 soup = BeautifulSoup(html_content, 'html.parser')
#                 tags = [tag.name for tag in soup.find_all()]
#                 tag_counts = Counter(tags)

#                 # Initialize dictionary for current file
#                 tag_counts_all = {tag: tag_counts[tag] if tag in tag_counts else 0 for tag in tags_of_interest}

#                 # Update tag counts for current file
#                 file_tag_counts[filename] = tag_counts_all
#     end_time = datetime.now() 
#     execution_time = end_time - start_time
#     print(f"process HTML file execution time: {execution_time}")
#     return file_tag_counts


def process_single_file(args: Tuple[str, str, List[str]]) -> Dict[str, Dict[str, int]]:
    """
    Process a single HTML file and count occurrences of specified tags.
    
    Parameters:
    - args (Tuple[str, str, List[str]]): A tuple containing the folder path, filename, 
      and a list of tags of interest.
    
    Returns:
    - Dict[str, Dict[str, int]]: A dictionary where the key is the filename and the value is another dictionary 
                                   with tag counts.
    """
    folder_path, filename, tags_of_interest = args
    file_path = os.path.join(folder_path, filename)
    tag_counts_all = {}
    
    if os.path.isfile(file_path):
        with open(file_path, 'r', encoding='utf-8') as file:
            html_content = file.read()
            # print(f"Processing file: {filename}")
            soup = BeautifulSoup(html_content, 'html.parser')
            tags = [tag.name for tag in soup.find_all()]
            tag_counts = Counter(tags)

            tag_counts_all = {tag: tag_counts[tag] if tag in tag_counts else 0 for tag in tags_of_interest}
    else:
        print(f"File {file_path} does not exist.")

    return {filename: tag_counts_all}

def process_html_files_p(folder_path: str, tags_of_interest: List[str]) -> Dict[str, Dict[str, int]]:
    """
    Processes HTML files in a folder and counts occurrences of specified tags.
    
    Parameters:
    - folder_path (str): The path to the folder containing HTML files.
    - tags_of_interest (List[str]): A list of tag names to count occurrences of in the HTML content.
    
    Returns:
    - Dict[str, Dict[str, int]]: A dictionary where keys are file names and values are dictionaries
    where keys are tag names from tags_of_interest and values are counts of their occurrences.
    >>> process_html_files_p('tests/test1', ['header', 'h1', 'section', 'h2', 'p', 'h3', 'li'])
    {'file1.html': {'header': 1, 'h1': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}, 'file2.html': {'header': 0, 'h1': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0, 'li': 2}}

    >>> process_html_files_p('tests/test1', ['header', 'section', 'h2', 'p', 'h3', 'li'])
    {'file1.html': {'header': 1, 'section': 2, 'h2': 2, 'p': 4, 'h3': 2, 'li': 6}, 'file2.html': {'header': 0, 'section': 1, 'h2': 1, 'p': 1, 'h3': 0, 'li': 2}}

    >>> process_html_files_p('tests/test2', ['li', 'header', 'section', 'h2', 'p', 'h3'])
    {'file3.html': {'li': 0, 'header': 0, 'section': 0, 'h2': 0, 'p': 0, 'h3': 0}, 'file4.html': {'li': 0, 'header': 0, 'section': 0, 'h2': 0, 'p': 1, 'h3': 0}}
    """
    file_tag_counts = {}
    files = [f for f in os.listdir(folder_path) if f.endswith('.html')]
    
    with Pool() as pool:
        args = [(folder_path, filename, tags_of_interest) for filename in files]
        results = pool.map(process_single_file, args) 

    for result in results:
        file_tag_counts.update(result)

    return file_tag_counts

# def transform_tag_counts_to_lists(file_tag_counts: Dict[str, Dict[str, int]]) -> Dict[str, List[int]]:
#     """
#     Examples:
#     >>> transform_tag_counts_to_lists({'file1': {'tag1': 10, 'tag2': 20}, 'file2': {'tag1': 15, 'tag2': 25}})
#     {'file1': [10, 20], 'file2': [15, 25]}
#     >>> transform_tag_counts_to_lists({'file1': {'tag1': 5, 'tag2': 10, 'tag3': 15}})
#     {'file1': [5, 10, 15]}
#     >>> transform_tag_counts_to_lists({'file1': {'tag1': 0, 'tag2': 0}})
#     {'file1': [0, 0]}

#     # these test to make sure that the list returned still has a consistent order even if tags
#     # input are originally not all input in the same order (this may happen given that
#     # we are using dictionaries, which are not inherently ordered)
#     >>> transform_tag_counts_to_lists({'file1': {'tag2': 20, 'tag1': 10}, 'file2': {'tag1': 15, 'tag2': 25}})
#     {'file1': [10, 20], 'file2': [15, 25]}
#     >>> transform_tag_counts_to_lists({'file1': {'tag2': 20, 'tag1': 10}, 'file2': {'tag2': 25, 'tag1': 15}})
#     {'file1': [10, 20], 'file2': [15, 25]}
#     >>> transform_tag_counts_to_lists({'file1': {'tag2': 20, 'tag1': 10, 'grays': 6, 'zebra': 100}, 'file2': {'grays': 6, 'zebra': 100, 'tag2': 20, 'tag1': 10}})
#     {'file1': [6, 10, 20, 100], 'file2': [6, 10, 20, 100]}

#     Transforms tag counts from dictionary format to a dictionary of lists format.

#     Parameters:
#     - file_tag_counts (Dict[str, Dict[str, int]]): A dictionary where keys are file names and values are dictionaries containing tag counts.

#     Returns:
#     - Dict[str, List[int]]: A dictionary where keys are file names and values are lists of integers representing tag counts.
#     """
#     transformed_counts: Dict[str, List[int]] = {}

#     for filename, tag_counts in file_tag_counts.items():
#         tags = sorted(tag_counts.keys())
#         transformed_counts[filename] = [tag_counts[tag] for tag in tags]

#     return transformed_counts

def process_file_tag_counts(item: Tuple[str, Dict[str, int]]) -> Tuple[str, List[int]]:
    """
    Helper function to transform tag counts for a single file.

    Parameters:
    - item (Tuple[str, Dict[str, int]]): A tuple containing the filename and its corresponding tag counts.

    Returns:
    - Tuple[str, List[int]]: A tuple containing the filename and the corresponding list of tag counts.
    >>> process_file_tag_counts(('file1', {'tag1': 10, 'tag2': 20}))
    ('file1', [10, 20])
    
    >>> process_file_tag_counts(('file2', {'tag3': 30, 'tag1': 15, 'tag2': 25}))
    ('file2', [15, 25, 30])
    
    >>> process_file_tag_counts(('file3', {'tag1': 5, 'tag2': 5, 'tag3': 10}))
    ('file3', [5, 5, 10])
    """
    filename, tag_counts = item
    tags = sorted(tag_counts.keys())
    counts = [tag_counts[tag] for tag in tags]

    return filename, counts

def transform_tag_counts_to_lists_p(file_tag_counts: Dict[str, Dict[str, int]]) -> Dict[str, List[int]]:
    """
    Transforms tag counts from dictionary format to a dictionary of lists format.

    Parameters:
    - file_tag_counts (Dict[str, Dict[str, int]]): A dictionary where keys are file names and values are dictionaries containing tag counts.

    Returns:
    - Dict[str, List[int]]: A dictionary where keys are file names and values are lists of integers representing tag counts.
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 10, 'tag2': 20}, 'file2': {'tag1': 15, 'tag2': 25}})
    {'file1': [10, 20], 'file2': [15, 25]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 5, 'tag2': 10, 'tag3': 15}})
    {'file1': [5, 10, 15]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 0, 'tag2': 0}})
    {}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag2': 20, 'tag1': 10}, 'file2': {'tag1': 15, 'tag2': 25}})
    {'file1': [10, 20], 'file2': [15, 25]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag2': 20, 'tag1': 10}, 'file2': {'tag2': 25, 'tag1': 15}})
    {'file1': [10, 20], 'file2': [15, 25]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag2': 20, 'tag1': 10, 'grays': 6, 'zebra': 100}, 'file2': {'grays': 6, 'zebra': 100, 'tag2': 20, 'tag1': 10}})
    {'file1': [6, 10, 20, 100], 'file2': [6, 10, 20, 100]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 0, 'tag2': 0}, 'file2': {'tag1': 15, 'tag2': 25}})
    {'file2': [15, 25]}
    >>> transform_tag_counts_to_lists_p({'file1': {'tag1': 0, 'tag2': 0}, 'file2': {'tag1': 15, 'tag2': 25}, 'file3' : {'tag1': 0, 'tag2': 0}, 'file4' : {'tag1': 0, 'tag2': 0}})
    {'file2': [15, 25]}
    """
    with concurrent.futures.ProcessPoolExecutor() as executor:
        results = executor.map(process_file_tag_counts, file_tag_counts.items())

    transformed_counts = dict(results)
    # getting rid of all tag vectors full of all 0s because they cause an issue with wpd 
    # inherent to wpd definition 
    filtered_counts = {filename: counts for filename, counts in transformed_counts.items() if not all(count == 0 for count in counts)}
    
    return filtered_counts

def map_lists_to_strings(tag_counts: Dict[str, List[int]]) -> Tuple[Dict[tuple, List[str]], List[tuple]]:
    """
    >>> map_lists_to_strings({'file1': [1,2,3], 'file2': [1,2,4]})
    ({(1, 2, 3): ['file1'], (1, 2, 4): ['file2']}, [(1, 2, 3), (1, 2, 4)])
    >>> map_lists_to_strings({'file1': [1,2,3], 'file2': [1,2,3]})
    ({(1, 2, 3): ['file1', 'file2']}, [(1, 2, 3)])
    >>> map_lists_to_strings({'file2': [1,2,3], 'file1': [1,2,3]})
    ({(1, 2, 3): ['file2', 'file1']}, [(1, 2, 3)])
    >>> map_lists_to_strings({})
    ({}, [])
    >>> map_lists_to_strings({'file1': [1,2,3], 'file2': [1,2,4], 'file3': [1,2,3], 'file4' : [1,2,4]})
    ({(1, 2, 3): ['file1', 'file3'], (1, 2, 4): ['file2', 'file4']}, [(1, 2, 3), (1, 2, 4)])
    """
    reversed_mapping = {}
    unique_counts = set()
    for filename, counts in tag_counts.items():
        counts_tuple = tuple(counts) 
        unique_counts.add(counts_tuple)
        if counts_tuple in reversed_mapping:
            reversed_mapping[counts_tuple].append(filename)
        else:
            reversed_mapping[counts_tuple] = [filename]
    return reversed_mapping, list(unique_counts)

def weighted_dif(list1: List[int], list2 : List[int]) -> float:
    """""
    >>> weighted_dif([1, 2, 3], [4, 5, 6])
    1.85
    >>> weighted_dif([0], [0])
    0.0
    >>> weighted_dif([1,2], [1,2])
    0.0
    >>> weighted_dif([1,1,1], [0,0,0])
    3.0
    >>> weighted_dif([1,0,1], [0,0,0])
    2.0
    >>> weighted_dif([1, 2, 3], [4, 5])  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    ValueError: Input lists must have the same length.

    Calculate the weighted difference as defined in Cui's thesis. 

    Parameters:
    list1 (list of int): First list of integers. Here, it is the list of
    ints representing the number of times each HTML tag from our corpus is
    found in a specific webpage. 

    list2 (list of int): Second list of integers. Here, it is the list of
    ints representing the number of times each HTML tag from our corpus is
    found in a specific webpage. 

    Returns:
    float: The calculated weighted difference between the two lists.
    """

    # check to ensure lists are of the same length 
    # if they aren't, there is something wrong with how they were formed
    # as every list here have a length equal to the number of elements
    # in the tags_of_interest list
    if len(list1) != len(list2):
        raise ValueError("Input lists must have the same length.")

    # Initialize the total score
    total_score : float = 0.0

    # Iterate through the lists one at a time
    for num1, num2 in zip(list1, list2):
        # Calculate the absolute difference between the numbers
        absolute_diff : float = abs(num1 - num2)
        
        # Calculate the maximum value between the two numbers
        max_value = max(num1, num2)
        
        # Avoid division by zero (doing this is okay bc if max_value is zero
        # then this implies that values at list[indice] is 0 for both list1 and
        # list 2 and so the difference is equal to 0)
        if max_value == 0:
            continue
        
        # Calculate the score for the current index
        score : float = absolute_diff / max_value
        
        # Add the score to the total score
        total_score += score

    return total_score


def EQU(num1: int, num2: int) -> int:
    """
    >>> EQU(0, 0)
    0
    >>> EQU(0, 1)
    0
    >>> EQU(1, 0)
    0
    >>> EQU(1, 1)
    1
    >>> EQU(2, 2)
    1
    >>> EQU(-1, -1)
    1
    >>> EQU(-1, 1)
    0
    >>> EQU(1, 2)
    0

    Check if two integers are equal and not equal to 0.

    Parameters:
    num1 (int): First integer.
    num2 (int): Second integer.

    Returns:
    int: 1 if the integers are equal to each other and not equal to 0,
    returns 0 otherwise.
    """
    if num1 == num2 and num1 != 0:
        return 1
    else:
        return 0

def S(list1: List[int], list2: List[int]) -> int:
    """
    >>> S([1, 2, 3], [1, 2, 3])
    3
    >>> S([1, 1, 1], [0, 0, 0])
    0
    >>> S([0, 0, 0], [1, 1, 1])
    0
    >>> S([1, 1, 1], [1, 1, 1])
    3
    >>> S([0], [0])
    0
    >>> S([1, 2, 0, 0, 12, 15, 16], [1, 2, 0, 0, 1, 2, 35])
    2

    Sum the results of running EQU on two lists of integers.

    Parameters:
    list1 (List[int]): First list of integers.
    list2 (List[int]): Second list of integers.

    Returns:
    int: Sum of the results of running EQU.
    """  
    total: int = 0
    for num1, num2 in zip(list1, list2):
        total += EQU(num1, num2)
    return total

def wpd (list1: List[int], list2: List[int]) -> float:
   """
    Calculate the weighted proportion difference as defined in Cui's paper.

    Parameters:
    list1 (List[int]): First list of integers.
    list2 (List[int]): Second list of integers.

    Returns:
    float: The weighted proportion difference between list1 and list2.

    >>> wpd([0], [0])
    0.0
    >>> wpd([1,2], [1,2])
    0.0
    >>> wpd([1, 2, 3], [1, 2, 3])
    0.0
    >>> wpd([1, 1, 1], [0, 0, 0])
    1.0
    >>> wpd([0, 0, 0], [1, 1, 1])
    1.0
    >>> wpd([1, 1, 1], [1, 1, 1])
    0.0
    >>> wpd([1, 2, 0, 0, 12, 15, 16], [1, 2, 0, 0, 1, 2, 35])
    0.5376995046780407
    >>> wpd([1, 2, 3], [4, 5, 6])
    1.0
   """
  # calculate the weighted difference 
   wd : float = weighted_dif(list1, list2)

  # calcuate the sum that is included in the denominator 
   s_val : int = S(list1, list2)

  # calculate the denominator 
   denominator : float = wd + s_val 

  # Avoid division by zero
  # I think this is a reasonable thing to do here, not sure what the alt would be
   if denominator == 0:
      return 0.0

   wpd_value: float = wd / denominator

   return wpd_value 

def D(list1: List[int], list2 : List[int]) -> float:
    """
    >>> D([0, 0, 0], [1, 1, 1])
    3
    >>> D([1, 1, 1], [1, 1, 1])
    0
    >>> D([1, 2, 3], [1, 1, 1])
    2
    >>> D([0, 0, 0, 4], [1, 1, 1])
    Traceback (most recent call last):
    ...
    ValueError: Lists should have the same length
    """
    if len(list1) != len(list2):
        raise ValueError("Lists should have the same length")

    return sum(1 if list1[i] != list2[i] else 0 for i in range(len(list1))) 

def L(list1: List[int], list2 : List[int]) -> float:
    """
    >>> L([0, 0, 0], [1, 1, 1])
    3
    >>> L([1, 1, 1], [1, 1, 1])
    3
    >>> L([1, 2, 3], [1, 1, 1])
    3
    >>> L([0, 0, 0], [0, 0, 0])
    0
    >>> L([0, 1, 0], [0, 0, 0])
    1
    >>> L([0, 0, 0, 4], [1, 1, 1])
    Traceback (most recent call last):
    ...
    ValueError: Lists should have the same length
    """
    if len(list1) != len(list2):
        raise ValueError("Lists should have the same length")
    return sum(1 if list1[i]!= 0 or list2[i]!=0 else 0 for i in range(len(list1)))

def pd (list1: List[int], list2: List[int]) -> float:
    """
    >>> pd([1, 2, 3, 4, 5, 0, 0, 0, 0], [1, 0, 0, 0, 0, 6, 7, 0, 0])
    0.8571428571428571
    >>> pd([0, 0, 0, 4], [1, 1, 1])
    Traceback (most recent call last):
    ...
    ValueError: Lists should have the same length
    """
    d = D(list1, list2)
    l = L(list1, list2)
    pds = d/l
    return pds 

def compute_distance_matrix_unique(unique_counts: List[Tuple[int]], batch_size: int = 1) -> List[List[float]]:
    """
    Computes the distance matrix using wpd for unique tag vectors.

    Parameters:
    - unique_counts (List[Tuple[int]]): List of unique tag vectors.
    - batch_size (int): Number of pairs of distances to compute per batch.

    Returns:
    - List[List[float]]: A matrix representing the distances between unique tag vectors.

    >>> compute_distance_matrix_unique([(10, 20), (15, 25)])
    [[0.0, 1.0], [1.0, 0.0]]
    >>> compute_distance_matrix_unique([(15, 25), (10, 20)])
    [[0.0, 1.0], [1.0, 0.0]]
    >>> compute_distance_matrix_unique([(6, 10, 20, 100), (6, 10, 20, 100)])
    Traceback (most recent call last):
    ...
    AssertionError: num_unique is not equal to the number of unique counts. bad input
    >>> compute_distance_matrix_unique([(1, 3, 30, 13), (6, 10, 20, 100), (11, 16, 2, 50)])
    [[0.0, 1.0, 1.0], [1.0, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(1, 3, 30, 13), (1, 10, 20, 100), (11, 16, 2, 50)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(11, 16, 2, 50), (1, 3, 30, 13), (1, 10, 20, 100)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(1, 3, 30, 13), (1, 10, 20, 100), (1, 3, 30, 13)])
    Traceback (most recent call last):
    ...
    AssertionError: num_unique is not equal to the number of unique counts. bad input
    >>> compute_distance_matrix_unique([(11, 16, 2, 50), (1, 3, 30, 13), (1, 10, 20, 100)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(11, 16, 2, 50), (1, 10, 20, 100), (1, 3, 30, 13)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(1, 10, 20, 100), (1, 3, 30, 13), (11, 16, 2, 50)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    """
    num_unique = len(unique_counts)
    sorted_unique_counts = sorted(unique_counts)
    assert num_unique == len(set(unique_counts)), "num_unique is not equal to the number of unique counts. bad input"

    # Initialize the distance matrix for unique vectors
    distance_matrix: List[List[float]] = [[0.0] * num_unique for _ in range(num_unique)]
    
    batches = [(i, j) for i in range(num_unique) for j in range(i + 1, num_unique)]

    with Pool(processes=os.cpu_count()) as pool:
        #distance_metric
        args = [(sorted_unique_counts, batches[i:i + batch_size],) for i in range(0, len(batches), batch_size)]
        
        results = pool.map(compute_batch_distances_unique, args)

        for batch_results in results:
            for i, j, distance in batch_results:
                distance_matrix[i][j] = distance
                distance_matrix[j][i] = distance 

    return distance_matrix

# Callable[[List[int], List[int]], float]]
def compute_batch_distances_unique(args: Tuple[List[Tuple[int]], List[Tuple[int, int]]]) -> List[Tuple[int, int, float]]:
    """
    Computes distances for a batch of unique vector pairs.

    Parameters:
    - args (Tuple[List[Tuple[int]], List[Tuple[int, int]]]): A tuple containing the list of unique tag vectors,
      and a batch of vector index pairs.

    Returns:
    - List[Tuple[int, int, float]]: A list of tuples containing indices and their computed distances.
    """
    # distance_fun
    unique_counts, batch = args
    results = []
    for i, j in batch:
        distance = wpd(unique_counts[i], unique_counts[j])  
        results.append((i, j, distance))
    return results

def random_permute_distance_matrix(distance_matrix: list[list[float]]) -> list[list[float]]:
    indices = list(range(len(distance_matrix)))
    random.shuffle(indices)  
    # print(indices)
    
    permuted_matrix = [[distance_matrix[i][j] for j in indices] for i in indices]
    
    return permuted_matrix


# def compute_distance_matrix(data_dict: Dict[str, List[int]]) -> List[List[float]]:
#     """
#     Computes the distance matrix using wpd for every combination of keys in the
#     dictionary that contains each list of ints that represents the number of HTML
#     tags in a given webpage (keeping in mind that the distance between 
#     (a and b) and (b and a) is the same thing, so we only need one of them to know
#     both. 
      
#     Parameters:
#     - data_dict (Dict[str, List[int]]): The dictionary that contains all lists of
#       ints that represents the number of HTML tags in a given webpage. 

#     Returns:
#     - List[List[float]]: A matrix representing the distances between HTML tag
#     occurances, with distance being defined as weighted proportional distance (wpd).

#     >>> compute_distance_matrix({'file1': [10, 20], 'file2': [15, 25]})
#     [[0.0, 1.0], [1.0, 0.0]]
#     >>> compute_distance_matrix({'file2': [15, 25], 'file1': [10, 20]})
#     [[0.0, 1.0], [1.0, 0.0]]
#     >>> compute_distance_matrix({'file1': [6, 10, 20, 100], 'file2': [6, 10, 20, 100]})
#     [[0.0, 0.0], [0.0, 0.0]]
#     >>> compute_distance_matrix({'file2': [6, 10, 20, 100], 'file1': [6, 10, 20, 100]})
#     [[0.0, 0.0], [0.0, 0.0]]
#     >>> compute_distance_matrix({'file1': [1, 3, 30, 13], 'file2': [6, 10, 20, 100], 'file3': [11, 16, 2, 50]})
#     [[0.0, 1.0, 1.0], [1.0, 0.0, 1.0], [1.0, 1.0, 0.0]]
#     >>> compute_distance_matrix({'file1': [1, 3, 30, 13], 'file2': [1, 10, 20, 100], 'file3': [11, 16, 2, 50]})
#     [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
#     >>> compute_distance_matrix({'file3': [11, 16, 2, 50], 'file1': [1, 3, 30, 13], 'file2': [1, 10, 20, 100]})
#     [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
#     """
#     keys = sorted(data_dict.keys())  
#     #keys = list(data_dict.keys())
#     num_keys = len(keys)
    
#     distance_matrix = [[0.0] * num_keys for _ in range(num_keys)]
    
#     for i in range(num_keys):
#         key1 = keys[i]
#         for j in range(i + 1, num_keys):  # Start from i + 1 to avoid computing distances twice
#             key2 = keys[j]

#             # Compute the distance for the pair of keys
#             distance = wpd(data_dict[key1], data_dict[key2])

#             # Store the computed distance in the distance matrix
#             distance_matrix[i][j] = distance_matrix[j][i] = distance

#     assert is_matrix_sorted(data_dict, distance_matrix), "distance matrix isn't sorted"

#     return distance_matrix


def cluster_test(distance_matrix : List[List[float]], distance_threshold : float) -> List[int]:
    """
    returns the labels associated with agglomeratively clustering based on the values within 
    distance_matrix and a stopping point of the value of distance_threshold. 
    
    Parameters:
    - List[List[float]]: A matrix representing the distances between HTML tag
    occurances, with distance being defined as weighted proportional distance (wpd).
    -float : a float representing the distance threshold at which we stop further clustering

    -List[int] : a list of ints representing which cluster each component of the distance_matrix ended up in
    
    >>> cluster_test([[0.0, 1.0],[1.0, 0.0]], 0.6)
    [1, 0]
    >>> cluster_test([[0.0, 1.0],  [1.0, 0.0]], 1.5)
    [0, 0]
    >>> cluster_test([[0.0, 0.0], [0.0, 0.0]], 0.5)
    [0, 0]
    >>> cluster_test([[0.0, 0.6555683122847302, 1.0],[0.6555683122847302, 0.0, 1.0],[1.0, 1.0, 0.0]], 0.9)
    [0, 0, 1]
    >>> cluster_test([[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]], 0.7)
    [0, 0, 1]
    >>> cluster_test([[0.0, 1.0, 1.0], [1.0, 0.0, 1.0], [1.0, 1.0, 0.0]], 1.1)
    [0, 0, 0]
    """
    clustering = AgglomerativeClustering(n_clusters=None, metric = 'precomputed', linkage="average", distance_threshold = distance_threshold)
    clustering.fit(distance_matrix)
    labels = clustering.labels_ 
    labels_l = labels.tolist()
    #print(labels)
    return labels_l 

def create_file_label_mapping(file_tag_counts: Dict[str, Dict[str, int]], distance_matrix: List[List[float]], threshold: float) -> Dict[str, int]:
    """
    Create a mapping between file names and cluster labels.

    Parameters:
    - file_tag_counts (Dict[str, Dict[str, int]]): A dictionary containing file names as keys and tag counts as values.
    - distance_matrix (List[List[float]]): The distance matrix obtained from computing distances between files.
    - float : the distance threshold above which we stop clustering. needed because it is a parameter to
    the skitlearn agglomerative clustering function

    Returns:
    - Dict[str, int]: A dictionary mapping file names to cluster labels.

    Examples:
        >>> file_tag_counts = {'file1.html': {'tag1': 10, 'tag2': 20}, 'file2.html': {'tag1': 15, 'tag2': 25}}
        >>> distance_matrix = [[0.0, 1.0], [1.0, 0.0]]
        >>> create_file_label_mapping(file_tag_counts, distance_matrix, .5)
        {'file1.html': 1, 'file2.html': 0}
        >>> file_tag_counts = {'file1.html': {'tag1': 12, 'tag2': 12}, 'file2.html': {'tag1': 12, 'tag2': 12}}
        >>> distance_matrix = [[0.0, 0.0], [0.0, 0.0]]
        >>> create_file_label_mapping(file_tag_counts, distance_matrix, .5)
        {'file1.html': 0, 'file2.html': 0}
        >>> file_tag_counts = {'file2.html': {'tag1': 15, 'tag2': 25}, 'file1.html': {'tag1': 10, 'tag2': 20}}
        >>> distance_matrix = [[0.0, 1.0], [1.0, 0.0]]
        >>> create_file_label_mapping(file_tag_counts, distance_matrix, .5)
        {'file1.html': 1, 'file2.html': 0}
    """
    file_names = sorted(file_tag_counts.keys())
    cluster_labels = cluster_test(distance_matrix, threshold)
    file_label_mapping = dict(zip(file_names, cluster_labels))
    return file_label_mapping


def intercomp(labels : List[float], which_cluster : int, distance_matrix : List[List[float]]) -> float:
    """
    Calculate the inter-cluster comparison value for a specified cluster.

    This function calculates the inter-cluster comparison value for a specific cluster identified by `which_cluster`
    within a set of clusters represented by `labels`, based on the distances between data points in the cluster
    as defined in the `distance_matrix`.

    Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - which_cluster (int): The label of the cluster to analyze.
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.

    Returns:
    - float: The inter-cluster comparison value for the specified cluster.

    Examples:
        >>> labels = [0, 4, 0, 2, 3, 2, 1]
        >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.0, 0.7, 0.9],
        ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.6, 1.0],
        ...                    [0.5, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7],
        ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
        ...                    [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0],
        ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
        ...                    [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0]]
        >>> intercomp(labels, 0, distance_matrix)
        0.5
        >>> intercomp(labels, 1, distance_matrix)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements
        >>> intercomp(labels, 2, distance_matrix)
        0.4
        >>> intercomp(labels, 3, distance_matrix)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements
        >>> intercomp(labels, 4, distance_matrix)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements

        >>> labels_2 = [1, 0, 1, 0, 4, 0, 3, 2, 2, 2]
        >>> distance_matrix_2 =  [[0.0, 1.0, 0.3, 0.8, 1.0, 0.7, 0.9, 0.5, 1.0, .5],
        ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.5, 1.0, 1.0, 0.7, 1.0],
        ...                     [0.3, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7, 0.6, 1.0, .6],
        ...                     [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8, 1.0, 1.0, 1.0],
        ...                     [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0, 0.8, 0.5, .8],
        ...                      [0.7, 0.5, 0.8, 0.4, 0.9, 0.0, 0.6, 0.3, 0.7, .3],
        ...                     [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0, 1.0, 0.9, 1.0],
        ...                      [0.5, 1.0, 0.6, 1.0, 0.8, 0.3, 1.0, 0.0, 0.4, 0.0],
        ...                      [1.0, 0.7, 1.0, 1.0, 0.5, 0.7, 0.9, 0.4, 0.0, 0.0],
        ...                      [0.5, 1.0, 0.6, 1.0, 0.8, 0.3, 1.0, 0.0, 0.4, 0.0]]
        >>> intercomp(labels_2, 0, distance_matrix_2)
        0.5
        >>> intercomp(labels_2, 1, distance_matrix_2)
        0.3
        >>> intercomp(labels_2, 2, distance_matrix_2)
        0.13333333333333333
        >>> intercomp(labels_2, 3, distance_matrix_2)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements
        >>> intercomp(labels_2, 4, distance_matrix_2)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements
    """
    # create a dictionary where the keys are the clusters
    # (represented by zero indexed integers) and the 
    # values are the number of sites falling into that cluster
    cluster_size = Counter(labels)
    #print(cluster_size)

    # cluster label to analyze
    cluster_to_analyze = which_cluster 

    if cluster_size[cluster_to_analyze] < 2:
        raise ValueError("Shouldn't be applying intercomp to clusters smaller than 2 elements")
    
    # Find indices of data points belonging to the cluster to analyze
    indices_in_cluster = [i for i, label in enumerate(labels) if label == cluster_to_analyze]

   # Initialize sum of WPD values for the cluster
    total_wpd_sum = 0.0

    # Generate all combinations of data points within the cluster
    combinations_within_cluster = combinations(indices_in_cluster, 2)

    # Calculate WPD value for each combination
    for data_point1, data_point2 in combinations_within_cluster:
        wpd_value = distance_matrix[data_point1][data_point2]  
        # print(f"wpd value for {data_point1}, {data_point2} is {wpd_value}")
        total_wpd_sum = total_wpd_sum + wpd_value
        # print(f"total wpd sum at this point is {total_wpd_sum}")


    num_data_points_in_cluster = cluster_size[cluster_to_analyze]
    denom = num_data_points_in_cluster * (num_data_points_in_cluster - 1)
    # print(f"the number of datapoints in this cluster is {num_data_points_in_cluster}")
    # print(f"the denominator we are using is {denom}")

    fract = 2 / denom 
    # print(f"fraction we are dividing by is {fract}")


    fin_val = fract * total_wpd_sum
    return fin_val 

def comp (labels : List[float], distance_matrix :  List[List[float]]) -> float:
    """
    Calculates the comp value for the subset of clusters which have more than one element.

    Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.

    Returns:
    - float: The overall comp value of the clusters specified by labels 

    >>> labels = [0, 4, 0, 2, 3, 2, 1]
    >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.0, 0.7, 0.9],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.6, 1.0],
    ...                    [0.5, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7],
    ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
    ...                    [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0],
    ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
    ...                    [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0]]
    >>> comp(labels, distance_matrix)
    0.45

    >>> labels_2 = [1, 0, 1, 0, 4, 0, 3, 2, 2, 2]
    >>> distance_matrix_2 =  [[0.0, 1.0, 0.3, 0.8, 1.0, 0.7, 0.9, 0.5, 1.0, .5],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.5, 1.0, 1.0, 0.7, 1.0],
    ...                     [0.3, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7, 0.6, 1.0, .6],
    ...                     [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8, 1.0, 1.0, 1.0],
    ...                     [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0, 0.8, 0.5, .8],
    ...                      [0.7, 0.5, 0.8, 0.4, 0.9, 0.0, 0.6, 0.3, 0.7, .3],
    ...                     [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0, 1.0, 0.9, 1.0],
    ...                      [0.5, 1.0, 0.6, 1.0, 0.8, 0.3, 1.0, 0.0, 0.4, 0.0],
    ...                      [1.0, 0.7, 1.0, 1.0, 0.5, 0.7, 0.9, 0.4, 0.0, 0.0],
    ...                      [0.5, 1.0, 0.6, 1.0, 0.8, 0.3, 1.0, 0.0, 0.4, 0.0]]
    >>> comp(labels_2, distance_matrix_2)
    0.3111111111111111
    
    
    >>> distance_matrix_3 = [[0.0, 1.0],[1.0, 0.0]]
    >>> labels_3 =  [1, 0]
    >>> comp(labels_3, distance_matrix_3)
    inf

    """
    total_intercomp_sum = 0.0
    cluster_size = Counter(labels)
    num_clusters_2 = sum(1 for size in cluster_size.values() if size >= 2)

    if num_clusters_2 == 0:
        return float('inf') 


    for cluster_label in cluster_size.keys():
        indices_in_cluster = [i for i, label in enumerate(labels) if label == cluster_label]
        if len(indices_in_cluster) >= 2:
            intercomp_value = intercomp(labels, cluster_label, distance_matrix)
            total_intercomp_sum = intercomp_value + total_intercomp_sum

    fract = 1/num_clusters_2

    final_comp = fract * total_intercomp_sum

    return final_comp

def smallest_wpd_between_clusters(labels: List[int], cluster1: int, cluster2: int, distance_matrix: List[List[float]]) -> float:
    """"
    returns the smallest wpd found between points from cluster1 and cluster2. analagous to intermin from
    Cui's thesis. 

    Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - cluster1: int and cluster2: int : Clusters, numbered as based on their label
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.

    - float : the smallest weighted proportial distance that exists between a point found in
    cluster1 and a point found in cluster2. 

    >>> labels = [0, 4, 0, 2, 3, 2, 1]
    >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.0, 0.7, 0.9],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.6, 1.0],
    ...                    [0.5, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7],
    ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
    ...                    [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0],
    ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
    ...                    [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0]]
    >>> smallest_wpd_between_clusters(labels, 0, 1, distance_matrix)
    0.7
    >>> smallest_wpd_between_clusters(labels, 0, 2, distance_matrix)
    0.7
    >>> smallest_wpd_between_clusters(labels, 0, 3, distance_matrix)
    1.0
    >>> smallest_wpd_between_clusters(labels, 0, 4, distance_matrix)
    0.9
    >>> smallest_wpd_between_clusters(labels, 4, 0, distance_matrix)
    0.9
    >>> smallest_wpd_between_clusters(labels, 0, 0, distance_matrix)
    0.0
    """
    # Find indices of data points belonging to cluster1 and cluster2
    indices_cluster1 = [i for i, label in enumerate(labels) if label == cluster1]
    indices_cluster2 = [i for i, label in enumerate(labels) if label == cluster2]

    # Initialize the smallest WPD value to infinity 
    smallest_wpd = float('inf')
    # Iterate over all pairs of data points, one from cluster1 and one from cluster2
    for index1 in indices_cluster1:
        #print(index1)
        for index2 in indices_cluster2:
            #print(index2)
            wpd_value = distance_matrix[index1][index2] 
            #print(wpd_value)
            #label1 = labels[index1]  # Get the label for index1
            #label2 = labels[index2]  # Get the label for index2
            #print(f"Index1: {index1}, Label1: {label1} | Index2: {index2}, Label2: {label2} | WPD Value: {wpd_value}")
            #print(f"the wpd_value value of {index1} and {index2} is {wpd_value}")
            # Update smallest_wpd if the current WPD value is smaller
            smallest_wpd = min(smallest_wpd, wpd_value)

    return smallest_wpd



def min_smallest_wpd_across_clusters(labels: List[int], distance_matrix: List[List[float]]) -> float:
    """"
    returns the smallest wpd value found between two vectors in any two clusters within our data
    analagous to min from Cui's thesis. 

     Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.
    in our case, the distance matrix is based on wpd as a measure of distance. 

    - float : the smallest weighted proportial distance that exists between two vectors in any two clusters
    within our data. 

    >>> labels = [0, 4, 0, 2, 3, 2, 1]
    >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.0, 0.7, 0.9],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.6, 1.0],
    ...                    [0.5, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7],
    ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
    ...                    [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0],
    ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
    ...                    [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0]]
    >>> min_smallest_wpd_across_clusters(labels, distance_matrix)
    0.6

    >>> labels2 = [0, 4, 0, 2, 3, 5, 1]
    >>> min_smallest_wpd_across_clusters(labels2, distance_matrix)
    0.4

    >>> labels3 = [0, 0, 0, 0, 0, 0, 1]
    >>> min_smallest_wpd_across_clusters(labels3, distance_matrix)
    0.6

    # >>> labels4 = [0, 0, 0, 0, 0, 0, 0]
    # >>> min_smallest_wpd_across_clusters(labels4, distance_matrix)
    # Traceback (most recent call last):
    # ...
    # AssertionError: We only have 1 cluster that all vectors are falling into! Not good!
    """
    # Get unique cluster labels
    unique_clusters = set(labels)
    num_unique_clusters = len(unique_clusters)
    assert num_unique_clusters != 1, "We only have 1 cluster that all vectors are falling into! Not good!"

    # Initialize the minimum smallest WPD value to a large number
    min_smallest_wpd = float('inf')
    log_system_usage(f"before iterating through all pairs of clusters")

    # Iterate over all pairs of clusters
    for cluster1 in unique_clusters:
        for cluster2 in unique_clusters:
            if cluster1 != cluster2:  # Avoid comparing a cluster to itself
                # Calculate the smallest WPD value between cluster1 and cluster2
                #print(f"cluster 1:{cluster1}")
                #print(f"cluster 2:{cluster2}")
                smallest_wpd = smallest_wpd_between_clusters(labels, cluster1, cluster2, distance_matrix)
                #print(f"the smallest wpd between those two clusters was: {smallest_wpd}")
                # Update min_smallest_wpd if the current smallest WPD value is smaller
                min_smallest_wpd = min(min_smallest_wpd, smallest_wpd)

    log_system_usage(f"after iterating through all pairs of clusters")
    #assert min_smallest_wpd != 0.0, "The minimum smallest WPD value is zero."

    return min_smallest_wpd


def coupling_clustering(labels : List[int], distance_matrix: List[List[float]]) -> float:
    """ Find the value of coupling of clustering for a given distance matrix and list of labels. 
    
    Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.
    in our case, the distance matrix is based on wpd as a measure of distance. 

    - float : the coupling of clustering value, defined in Cui's thesis as being comp divided by min. 

    >>> labels = [0, 4, 0, 2, 3, 2, 1]
    >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.2, 0.7, 0.9],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.1, 0.6, 1.0],
    ...                    [0.5, 0.9, 0.0, 0.7, 1.3, 0.8, 0.7],
    ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
    ...                    [1.2, 1.1, 1.3, 1.0, 0.0, 0.9, 1.1],
    ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
    ...                    [0.9, 1.0, 0.7, 0.8, 1.1, 0.6, 0.0]]
    >>> coupling_clustering(labels, distance_matrix)
    0.75

    >>> labels_2 = [1, 0, 1, 0, 4, 0, 3, 2, 2, 2]
    >>> distance_matrix_2 =  [[0.0, 1.1, 0.3, 0.8, 1.2, 0.7, 0.9, 0.5, 1.0, 0.5],
    ...                    [1.1, 0.0, 0.9, 0.6, 1.0, 0.5, 1.1, 1.2, 0.7, 1.2],
    ...                     [0.3, 0.9, 0.0, 0.7, 1.3, 0.8, 0.7, 0.6, 1.1, .6],
    ...                     [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8, 1.1, 1.2, 1.1],
    ...                     [1.2, 1.0, 1.3, 1.0, 0.0, 0.9, 1.1, 0.8, 0.5, .8],
    ...                      [0.7, 0.5, 0.8, 0.4, 0.9, 0.0, 0.6, 0.3, 0.7, .3],
    ...                     [0.9, 1.1, 0.7, 0.8, 1.1, 0.6, 0.0, 1.2, 0.9, 1.2],
    ...                      [0.5, 1.2, 0.6, 1.1, 0.8, 0.3, 1.2, 0.0, 0.4, 0.0],
    ...                      [1.0, 0.7, 1.1, 1.2, 0.5, 0.7, 0.9, 0.4, 0.0, 0.0],
    ...                      [0.5, 1.2, 0.6, 1.1, 0.8, 0.3, 1.2, 0.0, 0.4, 0.0]]
    >>> coupling_clustering(labels_2, distance_matrix_2)
    1.0370370370370372
    """
    comp_value = comp(labels, distance_matrix)
    #print("The comp value is ")
    #print(comp_value)
    #print("The smallest wpd value is ")
    smallest_wpd_val = min_smallest_wpd_across_clusters(labels, distance_matrix)
    #print(smallest_wpd_val)
    cc_value = comp_value/smallest_wpd_val
    #print("the cc value is ")
    #print(cc_value)
    return cc_value


def compute_coupling_clustering_for_threshold(wmp_matrix: List[List[float]], threshold: float) -> float:
    """"
    Computes the coupling clustering value for a given threshold.
    """
    clustering_labels = cluster_test(wmp_matrix, threshold)
    coupling_clustering_val = coupling_clustering(clustering_labels, wmp_matrix)
    return coupling_clustering_val

def find_optimal_threshold(wmp_matrix: List[List[float]]) -> float:
    """
    Finds the optimal threshold that minimizes the coupling clustering value that
    occurs when it is used.

    Parameters:
    - (List[List[float]]): A matrix representing the distances between data points.
    in our case, the distance matrix is based on wpd as a measure of distance. 

    Returns:
    - float: The value of the optimal threshold, from the hardcoded thresholds within the function.

    # these were all tested at  thresholds = values = [0.01 + 0.1 * i for i in range(10)]
    # >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.2, 0.7, 0.9],
    # ...                    [1.0, 0.0, 0.9, 0.6, 1.1, 0.6, 1.0],
    # ...                    [0.5, 0.9, 0.0, 0.7, 1.3, 0.8, 0.7],
    # ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
    # ...                    [1.2, 1.1, 1.3, 1.0, 0.0, 0.9, 1.1],
    # ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
    # ...                    [0.9, 1.0, 0.7, 0.8, 1.1, 0.6, 0.0]]
    # >>> find_optimal_threshold(distance_matrix)
    # .51

    # >>> distance_matrix_2 = [[0.0, 1.1, 0.3, 0.8, 1.2, 0.7, 0.9, 0.5, 1.0, 0.5],
    # ...                     [1.1, 0.0, 0.9, 0.6, 1.0, 0.5, 1.1, 1.2, 0.7, 1.2],
    # ...                     [0.3, 0.9, 0.0, 0.7, 1.3, 0.8, 0.7, 0.6, 1.1, 0.6],
    # ...                     [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8, 1.1, 1.2, 1.1],
    # ...                     [1.2, 1.0, 1.3, 1.0, 0.0, 0.9, 1.1, 0.8, 0.5, 0.8],
    # ...                     [0.7, 0.5, 0.8, 0.4, 0.9, 0.0, 0.6, 0.3, 0.7, 0.3],
    # ...                     [0.9, 1.1, 0.7, 0.8, 1.1, 0.6, 0.0, 1.2, 0.9, 1.2],
    # ...                     [0.5, 1.2, 0.6, 1.1, 0.8, 0.3, 1.2, 0.0, 0.0, 0.0],
    # ...                     [1.0, 0.7, 1.1, 1.2, 0.5, 0.7, 0.9, 0.0, 0.0, 0.0],
    # ...                     [0.5, 1.2, 0.6, 1.1, 0.8, 0.3, 1.2, 0.0, 0.0, 0.0]]
    # >>> find_optimal_threshold(distance_matrix_2)
    # .21000000000000002
    """
    # Define a range of thresholds to test
    thresholds = np.linspace(0.0, 1.0, 100)


    # Initialize variables to store the optimal threshold and its corresponding coupling clustering
    # this was previously 1, I think neg infinity makes more sense but might need to change back
    optimal_threshold = float('-inf')
    min_coupling_clustering = float('inf')
    coupling_clustering_values = []

    # with concurrent.futures.ProcessPoolExecutor() as executor:
    #     coupling_clustering_values = list(executor.map(compute_coupling_clustering_for_threshold, [wmp_matrix] * len(thresholds), thresholds))

    # for threshold, coupling_clustering_val in zip(thresholds, coupling_clustering_values):
    #     if (coupling_clustering_val < min_coupling_clustering) or (coupling_clustering_val == min_coupling_clustering and threshold > optimal_threshold):
    #         optimal_threshold = threshold
    #         min_coupling_clustering = coupling_clustering_val

    # Iterate over each threshold
    for threshold in thresholds:
        #print("we are in the threshold value")
        #print(threshold)
        # Calculate the coupling clustering
        clustering_labels = cluster_test(wmp_matrix, threshold)
        #print("the labels here look like")
        # print(clustering_labels)
        coupling_clustering_val = coupling_clustering(clustering_labels, wmp_matrix)
        #print("The coupling clustering value of ")
        #print(threshold)
        #print(" is ")
        #print(coupling_clustering_val)

        # Update optimal threshold if the current coupling clustering is smaller
        if (coupling_clustering_val <= min_coupling_clustering) or (coupling_clustering_val == min_coupling_clustering and threshold > optimal_threshold):
            optimal_threshold = threshold
            min_coupling_clustering = coupling_clustering_val

        coupling_clustering_values.append(coupling_clustering_val)

    plt.figure(figsize=(8, 6))
    plt.plot(thresholds, coupling_clustering_values, marker='o', linestyle='-', color='b')
    plt.title('Elbow Curve: Coupling Clustering vs. Threshold')
    plt.xlabel('Threshold')
    plt.ylabel('Coupling Clustering Value')
    plt.title('Threshold vs Coupling Clustering')
    plt.grid(True)
    
    # Highlight the optimal threshold
    # plt.axvline(x=optimal_threshold, color='r', linestyle='--', label=f'Optimal Threshold: {optimal_threshold}')
    # plt.legend()
    plt.show()

    return optimal_threshold

def values_to_keys(d):
    """
    values_to_keys(d) = d', where d'(v) = [k_0, k_1,...], where d[k_i] = v.
    """

    dd = {}
    for (k, v) in d.items():
        if v in dd:
            dd[v].append(k)
        else:
            dd[v] = [k]

    return dd

def compute_cluster_compactness(merged_cluster, distance_matrix):
    indices = list(merged_cluster)
    if len(indices) < 2:
        return float('inf')
    total_wpd_sum = 0.0
    for data_point1, data_point2 in combinations(indices, 2):
        total_wpd_sum += distance_matrix[data_point1][data_point2]
    n = len(indices)
    return (2 / (n * (n - 1))) * total_wpd_sum

def find_threshold_and_graph(wmp_matrix: List[List[float]],  folder_path : str, linkage_type, output_dir : str) -> float:
    clustering = AgglomerativeClustering(
        n_clusters=None,
        linkage=linkage_type,
        compute_full_tree=True,
        distance_threshold=0,
        metric='precomputed'
    )
    logging.info("we're about to fit")
    clustering.fit(wmp_matrix)
    logging.info("we've fit our model based on our distance matrix")

    # print(wmp_matrix)
    children = clustering.children_
    size_of_children = len(children)
    # print("Size of children:", size_of_children)
    logging.info("we've found the children attribute")

    distances = clustering.distances_
    logging.info("we've found the distances attribute")

    # print(children)
    
    labels = list(range(len(wmp_matrix)))  
    # min_dist = min_smallest_wpd_across_clusters(labels, wmp_matrix)
    # logging.info("we've found the minimum smallest wpd across all clusters")

    
    cluster_mapping = {i: (i,) for i in range(len(wmp_matrix))}
    compactness_memo = {i: float('inf') for i in range(len(wmp_matrix))}
    min_qoc = float('inf')
    best_threshold = None
    qoc_values = []  
    threshold_values = []  
                
    log_system_usage(f"before finding qoc for all thresholds")

    for i in range(clustering.children_.shape[0]):

        child1, child2 = children[i]

        new_cluster_id = len(wmp_matrix) + i  
        # print(f"i is {i}")
        
        # if child1 < len(wmp_matrix) and child2 < len(wmp_matrix):
        #     merged_cluster = (child1, child2)
        #     new_label = new_cluster_id
        # else:
        #     merged_cluster = (cluster_mapping[child1] if child1 >= len(wmp_matrix) else (child1, )) + \
        #     (cluster_mapping[child2] if child2 >= len(wmp_matrix) else (child2,)) 
        #     new_label = new_cluster_id

        if child1 < len(wmp_matrix) and child2 < len(wmp_matrix):
            merged_cluster = (child1, child2)
            new_label = new_cluster_id
        else:
            merged_cluster = (cluster_mapping[child1] + cluster_mapping[child2])
            new_label = new_cluster_id

        # print(f"2 while i is {i}")
        for idx in merged_cluster:
            labels[idx] = new_label 
        
        cluster_mapping[new_cluster_id] = merged_cluster
        
        compactness_memo.pop(child1, None)
        compactness_memo.pop(child2, None)
        # print(f"3 while i is {i}")


        if child1 in cluster_mapping:
            del cluster_mapping[child1]
        if child2 in cluster_mapping:
            del cluster_mapping[child2]
        
        compactness_memo[new_cluster_id] = compute_cluster_compactness(merged_cluster, wmp_matrix)
        # print(f"4 while i is {i}")

        total_compactness = sum(compactness for cluster_id, compactness in compactness_memo.items()
                            if len(cluster_mapping[cluster_id]) >= 2)
        num_clusters_2 = sum(1 for cluster_id in compactness_memo if len(cluster_mapping[cluster_id]) >= 2)

        if num_clusters_2 == 0:
            current_comp = float('inf')
        else:
            current_comp = total_compactness / num_clusters_2
        # print(f"Merging clusters {merged_cluster} into {new_cluster_id}")
        # print(f"Current labels: {labels}")
        # print(f"Current cluster_mapping state: {cluster_mapping}") 
        debug_messages = []
        # print(f"5 while i is {i}")

        unique_labels = set(labels)
        if len(unique_labels) > 1:
            current_distance = distances[i]
            # print(current_distance)
            if i == len(distances) - 1 or distances[i + 1] > current_distance:
                # 99% sure about this, but might need to be changed
                min_dist_thresh = distances[i+1] if i+1 < len(distances) else None
                logging.debug(f"about to find qoc for comp value of {current_comp}")
                qoc = current_comp/min_dist_thresh
                logging.debug(f"found qoc and it is {qoc}")
                qoc_values.append(qoc)
                threshold_values.append(min_dist_thresh)
                # print("fcomp here is")
                # print(fcomp)
                # threshold = distances[i + 1] if i + 1 < len(distances) else None
                # if qoc > 1:
                #     print(f"qoc is greater than 1 at {min_dist_thresh}")
                #     print(f"the comp value here is {fcomp} and qoc here is {qoc}")
                # print(f"Minimum distance across clusters at distance {current_distance}: {min_dist}")
                # print(f"Quality of Clustering (qoc) at threshold {threshold}: {qoc}")
                if qoc < min_qoc:
                    min_qoc = qoc
                    best_threshold = min_dist_thresh
            else:
                # print(f"Continuing to merge at distance {current_distance}; skipping qoc calculation.")
                debug_messages.append(f"Skipped qoc calculation at distance {current_distance}")
        else:
            # print("All points are in a single cluster; skipping min_dist calculation.")
            debug_messages.append("All points are in a single cluster; skipping min_dist calculation.")
        # print(f"6 while i is {i}")

        # print(f"Current cluster_mapping state: {cluster_mapping}")

    logging.info("we are working on making our graph")
    filename = os.path.basename(folder_path)
    save_path = os.path.join(output_dir, f'qoc_versus_threshold_{filename}_{linkage_type}.png')
    plt.figure(figsize=(8, 6))
    plt.plot(threshold_values, qoc_values, marker='o', linestyle='-', color='b')
    plt.xlabel('Threshold')
    plt.ylabel('Quality of Clustering (qoc)')
    plt.title(f'NEW_Quality of Clustering vs Threshold ({linkage_type} linkage for {folder_path})')
    # plt.ylim(0.45, 0.6)
    # plt.xlim(0.1, 0.35)
    plt.grid(True)
    plt.savefig(save_path)
    # plt.show()
    # print("Final labels:", labels)  
    # print(f"Smallest qoc: {min_qoc} at threshold: {best_threshold}")  
    # print("7")
    return best_threshold


def analyze_labels(numbers: List[int]) -> Tuple[int, float]:
    if not numbers:
        raise ValueError("The list of numbers cannot be empty.")
    return max(numbers)


# Believe this is working, want to reclone the 1k html file directory and make sure it divides correctly
def split_data(directory, train_dir, test_dir, train_ratio=0.8, seed=42):
    random.seed(seed)
    files = os.listdir(directory)
    print(f"Original files in directory: {files}")
    random.shuffle(files)
    train_size = int(len(files) * train_ratio)
    train_files = files[:train_size]
    print(train_files)
    test_files = files[train_size:]
    print(test_files)
    os.makedirs(train_dir, exist_ok=True)
    os.makedirs(test_dir, exist_ok=True)
    for file in train_files:
        os.rename(os.path.join(directory, file), os.path.join(train_dir, file))
    for file in test_files:
        os.rename(os.path.join(directory, file), os.path.join(test_dir, file))

def combine_analysis (folder_path: str, output_dir: str) -> None:
    """
    Executes functions within the analysis module, processing HTML files 
    found in a folder (folder_path) and counting occurrences of specified tags
    (those found in corpus used in Cui's paper) in a nested dictionary format,
    then transforming tag countsfrom nested dictionary format to a dictionary
    of lists format. Then, produces a matrix representing the distances between HTML tag
    occurances, with distance being defined as weighted proportional distance (wpd). 

    Parameters:
    - folder_path (str): The path to the folder containing HTML files.
    """
    # qr = map_lists_to_strings({'list1' : [1,2,3]})
    # print(qr)
    # tester = compute_distance_matrix_unique([(1,2,3), (1,2,4)])
    # print(tester)


# #     #  ['cfile1.html', 'file1.html']
#     v1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 2, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0]

# #     # ['file2.html'],
#     v2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0] 

# #     # ['file3.html']
#     v3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

# #     # ['file4.html']
#     v4 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0]

# # order when sorted will be v3, v4, v2, v1

    # v1_v2 = wpd(v1,v2)
    # print("wpd between vector 2 and vector 3 is")
    # print(v1_v2)

    # v1_v3 = wpd(v1, v3)
    # print("wpd between vector 3 and vector 0 is ")
    # print(v1_v3)

    # v1_v4 = wpd(v1,v4)
    # print("wpd between vector 3 and vector 1 is ")
    # print(v1_v4)

    # v2_v3 = wpd(v2,v3)
    # print("wpd between vector 2 and vector 0 is ")
    # print(v2_v3)

    # v2_v4 = wpd(v2,v4)
    # print("wpd between vector 2 and vector 1 is ")
    # print(v2_v4)

    # v3_v4 = wpd(v3,v4)
    # print("wpd between vector 0 and 1 is ")
    # print(v3_v4)

    # test0 = wpd([0,0,0], [0,0,0])
    # print("wpd between 0 vectors is ")
    # print(test0)

    # print('starting')
    # files = [os.path.join(folder_path, f) for f in os.listdir(folder_path) if os.path.isfile(os.path.join(folder_path, f))]
    # print(files)


    # split_data(folder_path, '../data/train_html', '../data/test_html')


    # current_directory = os.getcwd()
    log_directory = output_dir
    base_log_filename = 'dataclustering.log'
    log_file_path = os.path.join(log_directory, base_log_filename)
    i = 1
    while os.path.exists(log_file_path):
        # print("getting in here")
        log_file_path = os.path.join(log_directory, f'dataclustering_{i}.log')
        i += 1

    print(f"our final log file is {log_file_path}")
    logging.basicConfig(filename=log_file_path, filemode='a',
                    level=logging.INFO,
                    format='%(levelname)s - %(asctime)s - %(message)s')
    tags_of_interest : List[str] = ["a", "abbr", "address", "area", "article", "aside", "audio", "b", "base", "bdi", "bdo", "blockquote", "br", "button", "canvas", "caption", "cite", "code", "col", "colgroup", "command", "datalist", "dd", "del", "details", "dfn", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6", "header", "hgroup", "hr", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li", "link", "map", "mark", "math", "menu", "meta", "meter", "nav", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "script", "section", "select", "small", "source", "span", "strong", "style", "sub", "summary", "sup", "svg", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "u", "ul", "var", "video", "wbr"]
    tag_counts = process_html_files_p(folder_path, tags_of_interest)
    logging.info(f"we're processing the folder path {folder_path}")
    logging.info("we've found tag counts")
    log_system_usage("After finding tag counts")
    # # print('found tag_counts')
    tlist = transform_tag_counts_to_lists_p(tag_counts)
    logging.info("we've transformed tag counts to lists")
    log_system_usage("After transforming tag counts")
    # # print("transformed tag counts to lists")
    # # print(tlist)
    # # print('transformed tag counts')
    reverse_map, tag_vecs = map_lists_to_strings(tlist)
    logging.info("we've mapped lists to strings")
    log_system_usage("After mapping lists to strings")


    # print("mapped lists to strings")
    # print("the reverse map looks like")
    # print(reverse_map)
    # print("the tag vectors in our dataset look like")
    # print(tag_vecs)


    matrix = compute_distance_matrix_unique(tag_vecs)
    logging.info("we've computed the distance matrix")
    log_system_usage("After computing distance matrix")
    # print("computed distance matrix")

    # print("found matrix")
    # og_threshold = find_optimal_threshold(matrix)
    # print("found og threshold graph")


    linkages = ['single', 'average', 'complete']
    for linkage in linkages:
        logging.info(f"we're using the {linkage} kind of linkage")
        better_threshold = find_threshold_and_graph(matrix, folder_path, linkage, output_dir)
        logging.info(f"we've found the best threshold for {linkage} type of linkage and it is {better_threshold}")
        log_system_usage(f"we're finished finding best threshold for {linkage}")


    # print("found modified threshold graph")
        # print(f"for linkage {linkage} our labels look like")


        labels = cluster_test(matrix, better_threshold)
        logging.info(f"the labels for this best threshold look like {labels}")
        log_system_usage(f"we finished finding labels for {linkage}")
        num_clust = analyze_labels(labels) + 1 
        logging.info(f"for {linkage} type of linkage the number of clusters when using the best threshold is {num_clust}")

        # print(f"for {linkage} we have found")
        # print(f"the number of clusters is {num_clust} + 1")
    print(f"we are done. we have saved our log file as {log_file_path}")
    logging.info(f"we are now finished")