from collections import Counter
from typing import List, Dict, Tuple
from itertools import combinations
import doctest 
import matplotlib.pyplot as plt
import numpy as np
from multiprocessing import Pool
from itertools import combinations
from system_usage import log_system_usage
# import sklearn.cluster import AgglomerativeClustering
import sklearn.cluster



def cluster_test(distance_matrix : List[List[float]], distance_threshold : float) -> List[int]:
    """
    returns the labels associated with agglomeratively clustering based on the values within 
    distance_matrix and a stopping point of the value of distance_threshold. 
    
    Parameters:
    - List[List[float]]: A matrix representing the distances between HTML tag
    occurances, with distance being defined as weighted proportional distance (wpd).
    -float : a float representing the distance threshold at which we stop further clustering

    -List[int] : a list of ints representing which cluster each component of the distance_matrix ended up in
    
    >>> cluster_test([[0.0, 1.0],[1.0, 0.0]], 0.6)
    [1, 0]
    >>> cluster_test([[0.0, 1.0],  [1.0, 0.0]], 1.5)
    [0, 0]
    >>> cluster_test([[0.0, 0.0], [0.0, 0.0]], 0.5)
    [0, 0]
    >>> cluster_test([[0.0, 0.6555683122847302, 1.0],[0.6555683122847302, 0.0, 1.0],[1.0, 1.0, 0.0]], 0.9)
    [0, 0, 1]
    >>> cluster_test([[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]], 0.7)
    [0, 0, 1]
    >>> cluster_test([[0.0, 1.0, 1.0], [1.0, 0.0, 1.0], [1.0, 1.0, 0.0]], 1.1)
    [0, 0, 0]
    """
    clustering = sklearn.cluster.AgglomerativeClustering(n_clusters=None, metric = 'precomputed', linkage="average", distance_threshold = distance_threshold)
    clustering.fit(distance_matrix)
    labels = clustering.labels_ 
    labels_l = labels.tolist()
    #print(labels)
    return labels_l 

def create_file_label_mapping(file_tag_counts: Dict[str, Dict[str, int]], distance_matrix: List[List[float]], threshold: float) -> Dict[str, int]:
    """
    Create a mapping between file names and cluster labels.

    Parameters:
    - file_tag_counts (Dict[str, Dict[str, int]]): A dictionary containing file names as keys and tag counts as values.
    - distance_matrix (List[List[float]]): The distance matrix obtained from computing distances between files.
    - float : the distance threshold above which we stop clustering. needed because it is a parameter to
    the skitlearn agglomerative clustering function

    Returns:
    - Dict[str, int]: A dictionary mapping file names to cluster labels.

    Examples:
    >>> file_tag_counts = {'file1.html': {'tag1': 10, 'tag2': 20}, 'file2.html': {'tag1': 15, 'tag2': 25}}
    >>> distance_matrix = [[0.0, 1.0], [1.0, 0.0]]
    >>> create_file_label_mapping(file_tag_counts, distance_matrix, .5)
    {'file1.html': 1, 'file2.html': 0}
    >>> file_tag_counts = {'file1.html': {'tag1': 12, 'tag2': 12}, 'file2.html': {'tag1': 12, 'tag2': 12}}
    >>> distance_matrix = [[0.0, 0.0], [0.0, 0.0]]
    >>> create_file_label_mapping(file_tag_counts, distance_matrix, .5)
    {'file1.html': 0, 'file2.html': 0}
    >>> file_tag_counts = {'file2.html': {'tag1': 15, 'tag2': 25}, 'file1.html': {'tag1': 10, 'tag2': 20}}
    >>> distance_matrix = [[0.0, 1.0], [1.0, 0.0]]
    >>> create_file_label_mapping(file_tag_counts, distance_matrix, .5)
    {'file1.html': 1, 'file2.html': 0}
    """
    file_names = sorted(file_tag_counts.keys())
    cluster_labels = cluster_test(distance_matrix, threshold)
    file_label_mapping = dict(zip(file_names, cluster_labels))
    return file_label_mapping

def intercomp(labels : List[float], which_cluster : int, distance_matrix : List[List[float]]) -> float:
    """
    Calculate the inter-cluster comparison value for a specified cluster.

    This function calculates the inter-cluster comparison value for a specific cluster identified by `which_cluster`
    within a set of clusters represented by `labels`, based on the distances between data points in the cluster
    as defined in the `distance_matrix`.

    Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - which_cluster (int): The label of the cluster to analyze.
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.

    Returns:
    - float: The inter-cluster comparison value for the specified cluster.

    Examples:
        >>> labels = [0, 4, 0, 2, 3, 2, 1]
        >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.0, 0.7, 0.9],
        ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.6, 1.0],
        ...                    [0.5, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7],
        ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
        ...                    [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0],
        ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
        ...                    [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0]]
        >>> intercomp(labels, 0, distance_matrix)
        0.5
        >>> intercomp(labels, 1, distance_matrix)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements
        >>> intercomp(labels, 2, distance_matrix)
        0.4
        >>> intercomp(labels, 3, distance_matrix)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements
        >>> intercomp(labels, 4, distance_matrix)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements

        >>> labels_2 = [1, 0, 1, 0, 4, 0, 3, 2, 2, 2]
        >>> distance_matrix_2 =  [[0.0, 1.0, 0.3, 0.8, 1.0, 0.7, 0.9, 0.5, 1.0, .5],
        ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.5, 1.0, 1.0, 0.7, 1.0],
        ...                     [0.3, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7, 0.6, 1.0, .6],
        ...                     [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8, 1.0, 1.0, 1.0],
        ...                     [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0, 0.8, 0.5, .8],
        ...                      [0.7, 0.5, 0.8, 0.4, 0.9, 0.0, 0.6, 0.3, 0.7, .3],
        ...                     [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0, 1.0, 0.9, 1.0],
        ...                      [0.5, 1.0, 0.6, 1.0, 0.8, 0.3, 1.0, 0.0, 0.4, 0.0],
        ...                      [1.0, 0.7, 1.0, 1.0, 0.5, 0.7, 0.9, 0.4, 0.0, 0.0],
        ...                      [0.5, 1.0, 0.6, 1.0, 0.8, 0.3, 1.0, 0.0, 0.4, 0.0]]
        >>> intercomp(labels_2, 0, distance_matrix_2)
        0.5
        >>> intercomp(labels_2, 1, distance_matrix_2)
        0.3
        >>> intercomp(labels_2, 2, distance_matrix_2)
        0.13333333333333333
        >>> intercomp(labels_2, 3, distance_matrix_2)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements
        >>> intercomp(labels_2, 4, distance_matrix_2)
        Traceback (most recent call last):
        ...
        ValueError: Shouldn't be applying intercomp to clusters smaller than 2 elements
    """
    # create a dictionary where the keys are the clusters
    # (represented by zero indexed integers) and the 
    # values are the number of sites falling into that cluster
    cluster_size = Counter(labels)
    #print(cluster_size)

    # cluster label to analyze
    cluster_to_analyze = which_cluster 

    if cluster_size[cluster_to_analyze] < 2:
        raise ValueError("Shouldn't be applying intercomp to clusters smaller than 2 elements")
    
    # Find indices of data points belonging to the cluster to analyze
    indices_in_cluster = [i for i, label in enumerate(labels) if label == cluster_to_analyze]

   # Initialize sum of WPD values for the cluster
    total_wpd_sum = 0.0

    # Generate all combinations of data points within the cluster
    combinations_within_cluster = combinations(indices_in_cluster, 2)

    # Calculate WPD value for each combination
    for data_point1, data_point2 in combinations_within_cluster:
        wpd_value = distance_matrix[data_point1][data_point2]  
        # print(f"wpd value for {data_point1}, {data_point2} is {wpd_value}")
        total_wpd_sum = total_wpd_sum + wpd_value
        # print(f"total wpd sum at this point is {total_wpd_sum}")


    num_data_points_in_cluster = cluster_size[cluster_to_analyze]
    denom = num_data_points_in_cluster * (num_data_points_in_cluster - 1)
    # print(f"the number of datapoints in this cluster is {num_data_points_in_cluster}")
    # print(f"the denominator we are using is {denom}")

    fract = 2 / denom 
    # print(f"fraction we are dividing by is {fract}")


    fin_val = fract * total_wpd_sum
    return fin_val 

def comp (labels : List[float], distance_matrix :  List[List[float]]) -> float:
    """
    Calculates the comp value for the subset of clusters which have more than one element.

    Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.

    Returns:
    - float: The overall comp value of the clusters specified by labels 

    >>> labels = [0, 4, 0, 2, 3, 2, 1]
    >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.0, 0.7, 0.9],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.6, 1.0],
    ...                    [0.5, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7],
    ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
    ...                    [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0],
    ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
    ...                    [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0]]
    >>> comp(labels, distance_matrix)
    0.45

    >>> labels_2 = [1, 0, 1, 0, 4, 0, 3, 2, 2, 2]
    >>> distance_matrix_2 =  [[0.0, 1.0, 0.3, 0.8, 1.0, 0.7, 0.9, 0.5, 1.0, .5],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.5, 1.0, 1.0, 0.7, 1.0],
    ...                     [0.3, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7, 0.6, 1.0, .6],
    ...                     [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8, 1.0, 1.0, 1.0],
    ...                     [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0, 0.8, 0.5, .8],
    ...                      [0.7, 0.5, 0.8, 0.4, 0.9, 0.0, 0.6, 0.3, 0.7, .3],
    ...                     [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0, 1.0, 0.9, 1.0],
    ...                      [0.5, 1.0, 0.6, 1.0, 0.8, 0.3, 1.0, 0.0, 0.4, 0.0],
    ...                      [1.0, 0.7, 1.0, 1.0, 0.5, 0.7, 0.9, 0.4, 0.0, 0.0],
    ...                      [0.5, 1.0, 0.6, 1.0, 0.8, 0.3, 1.0, 0.0, 0.4, 0.0]]
    >>> comp(labels_2, distance_matrix_2)
    0.3111111111111111
    
    
    >>> distance_matrix_3 = [[0.0, 1.0],[1.0, 0.0]]
    >>> labels_3 =  [1, 0]
    >>> comp(labels_3, distance_matrix_3)
    inf

    """
    total_intercomp_sum = 0.0
    cluster_size = Counter(labels)
    num_clusters_2 = sum(1 for size in cluster_size.values() if size >= 2)

    if num_clusters_2 == 0:
        return float('inf') 


    for cluster_label in cluster_size.keys():
        indices_in_cluster = [i for i, label in enumerate(labels) if label == cluster_label]
        if len(indices_in_cluster) >= 2:
            intercomp_value = intercomp(labels, cluster_label, distance_matrix)
            total_intercomp_sum = intercomp_value + total_intercomp_sum

    fract = 1/num_clusters_2

    final_comp = fract * total_intercomp_sum

    return final_comp

def smallest_wpd_between_clusters(labels: List[int], cluster1: int, cluster2: int, distance_matrix: List[List[float]]) -> float:
    """"
    returns the smallest wpd found between points from cluster1 and cluster2. analagous to intermin from
    Cui's thesis. 

    Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - cluster1: int and cluster2: int : Clusters, numbered as based on their label
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.

    - float : the smallest weighted proportial distance that exists between a point found in
    cluster1 and a point found in cluster2. 

    >>> labels = [0, 4, 0, 2, 3, 2, 1]
    >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.0, 0.7, 0.9],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.6, 1.0],
    ...                    [0.5, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7],
    ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
    ...                    [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0],
    ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
    ...                    [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0]]
    >>> smallest_wpd_between_clusters(labels, 0, 1, distance_matrix)
    0.7
    >>> smallest_wpd_between_clusters(labels, 0, 2, distance_matrix)
    0.7
    >>> smallest_wpd_between_clusters(labels, 0, 3, distance_matrix)
    1.0
    >>> smallest_wpd_between_clusters(labels, 0, 4, distance_matrix)
    0.9
    >>> smallest_wpd_between_clusters(labels, 4, 0, distance_matrix)
    0.9
    >>> smallest_wpd_between_clusters(labels, 0, 0, distance_matrix)
    0.0
    """
    # Find indices of data points belonging to cluster1 and cluster2
    indices_cluster1 = [i for i, label in enumerate(labels) if label == cluster1]
    indices_cluster2 = [i for i, label in enumerate(labels) if label == cluster2]

    # Initialize the smallest WPD value to infinity 
    smallest_wpd = float('inf')
    # Iterate over all pairs of data points, one from cluster1 and one from cluster2
    for index1 in indices_cluster1:
        #print(index1)
        for index2 in indices_cluster2:
            #print(index2)
            wpd_value = distance_matrix[index1][index2] 
            #print(wpd_value)
            #label1 = labels[index1]  # Get the label for index1
            #label2 = labels[index2]  # Get the label for index2
            #print(f"Index1: {index1}, Label1: {label1} | Index2: {index2}, Label2: {label2} | WPD Value: {wpd_value}")
            #print(f"the wpd_value value of {index1} and {index2} is {wpd_value}")
            # Update smallest_wpd if the current WPD value is smaller
            smallest_wpd = min(smallest_wpd, wpd_value)

    return smallest_wpd



def min_smallest_wpd_across_clusters(labels: List[int], distance_matrix: List[List[float]]) -> float:
    """"
    returns the smallest wpd value found between two vectors in any two clusters within our data
    analagous to min from Cui's thesis. 

     Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.
    in our case, the distance matrix is based on wpd as a measure of distance. 

    - float : the smallest weighted proportial distance that exists between two vectors in any two clusters
    within our data. 

    >>> labels = [0, 4, 0, 2, 3, 2, 1]
    >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.0, 0.7, 0.9],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.0, 0.6, 1.0],
    ...                    [0.5, 0.9, 0.0, 0.7, 1.0, 0.8, 0.7],
    ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
    ...                    [1.0, 1.0, 1.0, 1.0, 0.0, 0.9, 1.0],
    ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
    ...                    [0.9, 1.0, 0.7, 0.8, 1.0, 0.6, 0.0]]
    >>> min_smallest_wpd_across_clusters(labels, distance_matrix)
    0.6

    >>> labels2 = [0, 4, 0, 2, 3, 5, 1]
    >>> min_smallest_wpd_across_clusters(labels2, distance_matrix)
    0.4

    >>> labels3 = [0, 0, 0, 0, 0, 0, 1]
    >>> min_smallest_wpd_across_clusters(labels3, distance_matrix)
    0.6

    # >>> labels4 = [0, 0, 0, 0, 0, 0, 0]
    # >>> min_smallest_wpd_across_clusters(labels4, distance_matrix)
    # Traceback (most recent call last):
    # ...
    # AssertionError: We only have 1 cluster that all vectors are falling into! Not good!
    """
    # Get unique cluster labels
    unique_clusters = set(labels)
    num_unique_clusters = len(unique_clusters)
    assert num_unique_clusters != 1, "We only have 1 cluster that all vectors are falling into! Not good!"

    # Initialize the minimum smallest WPD value to a large number
    min_smallest_wpd = float('inf')
    log_system_usage(f"before iterating through all pairs of clusters")

    # Iterate over all pairs of clusters
    for cluster1 in unique_clusters:
        for cluster2 in unique_clusters:
            if cluster1 != cluster2:  # Avoid comparing a cluster to itself
                # Calculate the smallest WPD value between cluster1 and cluster2
                #print(f"cluster 1:{cluster1}")
                #print(f"cluster 2:{cluster2}")
                smallest_wpd = smallest_wpd_between_clusters(labels, cluster1, cluster2, distance_matrix)
                #print(f"the smallest wpd between those two clusters was: {smallest_wpd}")
                # Update min_smallest_wpd if the current smallest WPD value is smaller
                min_smallest_wpd = min(min_smallest_wpd, smallest_wpd)

    log_system_usage(f"after iterating through all pairs of clusters")
    #assert min_smallest_wpd != 0.0, "The minimum smallest WPD value is zero."

    return min_smallest_wpd


def coupling_clustering(labels : List[int], distance_matrix: List[List[float]]) -> float:
    """ Find the value of coupling of clustering for a given distance matrix and list of labels. 
    
    Parameters:
    - labels (List[float]): A list of cluster labels assigned to each data point.
    - distance_matrix (List[List[float]]): A matrix representing the distances between data points.
    in our case, the distance matrix is based on wpd as a measure of distance. 

    - float : the coupling of clustering value, defined in Cui's thesis as being comp divided by min. 

    >>> labels = [0, 4, 0, 2, 3, 2, 1]
    >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.2, 0.7, 0.9],
    ...                    [1.0, 0.0, 0.9, 0.6, 1.1, 0.6, 1.0],
    ...                    [0.5, 0.9, 0.0, 0.7, 1.3, 0.8, 0.7],
    ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
    ...                    [1.2, 1.1, 1.3, 1.0, 0.0, 0.9, 1.1],
    ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
    ...                    [0.9, 1.0, 0.7, 0.8, 1.1, 0.6, 0.0]]
    >>> coupling_clustering(labels, distance_matrix)
    0.75

    >>> labels_2 = [1, 0, 1, 0, 4, 0, 3, 2, 2, 2]
    >>> distance_matrix_2 =  [[0.0, 1.1, 0.3, 0.8, 1.2, 0.7, 0.9, 0.5, 1.0, 0.5],
    ...                    [1.1, 0.0, 0.9, 0.6, 1.0, 0.5, 1.1, 1.2, 0.7, 1.2],
    ...                     [0.3, 0.9, 0.0, 0.7, 1.3, 0.8, 0.7, 0.6, 1.1, .6],
    ...                     [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8, 1.1, 1.2, 1.1],
    ...                     [1.2, 1.0, 1.3, 1.0, 0.0, 0.9, 1.1, 0.8, 0.5, .8],
    ...                      [0.7, 0.5, 0.8, 0.4, 0.9, 0.0, 0.6, 0.3, 0.7, .3],
    ...                     [0.9, 1.1, 0.7, 0.8, 1.1, 0.6, 0.0, 1.2, 0.9, 1.2],
    ...                      [0.5, 1.2, 0.6, 1.1, 0.8, 0.3, 1.2, 0.0, 0.4, 0.0],
    ...                      [1.0, 0.7, 1.1, 1.2, 0.5, 0.7, 0.9, 0.4, 0.0, 0.0],
    ...                      [0.5, 1.2, 0.6, 1.1, 0.8, 0.3, 1.2, 0.0, 0.4, 0.0]]
    >>> coupling_clustering(labels_2, distance_matrix_2)
    1.0370370370370372
    """
    comp_value = comp(labels, distance_matrix)
    #print("The comp value is ")
    #print(comp_value)
    #print("The smallest wpd value is ")
    smallest_wpd_val = min_smallest_wpd_across_clusters(labels, distance_matrix)
    #print(smallest_wpd_val)
    cc_value = comp_value/smallest_wpd_val
    #print("the cc value is ")
    #print(cc_value)
    return cc_value


def compute_coupling_clustering_for_threshold(wmp_matrix: List[List[float]], threshold: float) -> float:
    """"
    Computes the coupling clustering value for a given threshold.
    """
    clustering_labels = cluster_test(wmp_matrix, threshold)
    coupling_clustering_val = coupling_clustering(clustering_labels, wmp_matrix)
    return coupling_clustering_val

# def find_optimal_threshold(wmp_matrix: List[List[float]]) -> float:
#     """
#     Finds the optimal threshold that minimizes the coupling clustering value that
#     occurs when it is used.

#     Parameters:
#     - (List[List[float]]): A matrix representing the distances between data points.
#     in our case, the distance matrix is based on wpd as a measure of distance. 

#     Returns:
#     - float: The value of the optimal threshold, from the hardcoded thresholds within the function.

#     # these were all tested at  thresholds = values = [0.01 + 0.1 * i for i in range(10)]
#     # >>> distance_matrix = [[0.0, 1.0, 0.5, 0.8, 1.2, 0.7, 0.9],
#     # ...                    [1.0, 0.0, 0.9, 0.6, 1.1, 0.6, 1.0],
#     # ...                    [0.5, 0.9, 0.0, 0.7, 1.3, 0.8, 0.7],
#     # ...                    [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8],
#     # ...                    [1.2, 1.1, 1.3, 1.0, 0.0, 0.9, 1.1],
#     # ...                    [0.7, 0.6, 0.8, 0.4, 0.9, 0.0, 0.6],
#     # ...                    [0.9, 1.0, 0.7, 0.8, 1.1, 0.6, 0.0]]
#     # >>> find_optimal_threshold(distance_matrix)
#     # .51

#     # >>> distance_matrix_2 = [[0.0, 1.1, 0.3, 0.8, 1.2, 0.7, 0.9, 0.5, 1.0, 0.5],
#     # ...                     [1.1, 0.0, 0.9, 0.6, 1.0, 0.5, 1.1, 1.2, 0.7, 1.2],
#     # ...                     [0.3, 0.9, 0.0, 0.7, 1.3, 0.8, 0.7, 0.6, 1.1, 0.6],
#     # ...                     [0.8, 0.6, 0.7, 0.0, 1.0, 0.4, 0.8, 1.1, 1.2, 1.1],
#     # ...                     [1.2, 1.0, 1.3, 1.0, 0.0, 0.9, 1.1, 0.8, 0.5, 0.8],
#     # ...                     [0.7, 0.5, 0.8, 0.4, 0.9, 0.0, 0.6, 0.3, 0.7, 0.3],
#     # ...                     [0.9, 1.1, 0.7, 0.8, 1.1, 0.6, 0.0, 1.2, 0.9, 1.2],
#     # ...                     [0.5, 1.2, 0.6, 1.1, 0.8, 0.3, 1.2, 0.0, 0.0, 0.0],
#     # ...                     [1.0, 0.7, 1.1, 1.2, 0.5, 0.7, 0.9, 0.0, 0.0, 0.0],
#     # ...                     [0.5, 1.2, 0.6, 1.1, 0.8, 0.3, 1.2, 0.0, 0.0, 0.0]]
#     # >>> find_optimal_threshold(distance_matrix_2)
#     # .21000000000000002
#     """
#     # Define a range of thresholds to test
#     thresholds = np.linspace(0.0, 1.0, 100)


#     # Initialize variables to store the optimal threshold and its corresponding coupling clustering
#     # this was previously 1, I think neg infinity makes more sense but might need to change back
#     optimal_threshold = float('-inf')
#     min_coupling_clustering = float('inf')
#     coupling_clustering_values = []

#     # with concurrent.futures.ProcessPoolExecutor() as executor:
#     #     coupling_clustering_values = list(executor.map(compute_coupling_clustering_for_threshold, [wmp_matrix] * len(thresholds), thresholds))

#     # for threshold, coupling_clustering_val in zip(thresholds, coupling_clustering_values):
#     #     if (coupling_clustering_val < min_coupling_clustering) or (coupling_clustering_val == min_coupling_clustering and threshold > optimal_threshold):
#     #         optimal_threshold = threshold
#     #         min_coupling_clustering = coupling_clustering_val

#     # Iterate over each threshold
#     for threshold in thresholds:
#         #print("we are in the threshold value")
#         #print(threshold)
#         # Calculate the coupling clustering
#         clustering_labels = cluster_test(wmp_matrix, threshold)
#         #print("the labels here look like")
#         # print(clustering_labels)
#         coupling_clustering_val = coupling_clustering(clustering_labels, wmp_matrix)
#         #print("The coupling clustering value of ")
#         #print(threshold)
#         #print(" is ")
#         #print(coupling_clustering_val)

#         # Update optimal threshold if the current coupling clustering is smaller
#         if (coupling_clustering_val <= min_coupling_clustering) or (coupling_clustering_val == min_coupling_clustering and threshold > optimal_threshold):
#             optimal_threshold = threshold
#             min_coupling_clustering = coupling_clustering_val

#         coupling_clustering_values.append(coupling_clustering_val)

#     plt.figure(figsize=(8, 6))
#     plt.plot(thresholds, coupling_clustering_values, marker='o', linestyle='-', color='b')
#     plt.title('Elbow Curve: Coupling Clustering vs. Threshold')
#     plt.xlabel('Threshold')
#     plt.ylabel('Coupling Clustering Value')
#     plt.title('Threshold vs Coupling Clustering')
#     plt.grid(True)
    
#     # Highlight the optimal threshold
#     # plt.axvline(x=optimal_threshold, color='r', linestyle='--', label=f'Optimal Threshold: {optimal_threshold}')
#     # plt.legend()
#     plt.show()

#     return optimal_threshold


def compute_cluster_compactness(merged_cluster : Tuple, distance_matrix : List[List[float]]):
    """ 
    Computes the compactness of a cluster 
    based on average pairwise distances btw all points in the cluster
    as given in distance_matrix. 

    Returns: float : The compactness of the cluster. Lower value indicates more compact cluster. If 
    cluster has less than 2 points we return 'inf' because compactness is undefined.  

    >>> distance_matrix = [
    ...     [0.0, 1.0, 2.0],
    ...     [1.0, 0.0, 1.5],
    ...     [2.0, 1.5, 0.0]
    ... ]
    >>> compute_cluster_compactness([0, 1], distance_matrix)
    1.0
    >>> compute_cluster_compactness([1, 2], distance_matrix)
    1.5
    >>> compute_cluster_compactness([0, 1, 2], distance_matrix)
    1.5
    >>> compute_cluster_compactness([0], distance_matrix)
    inf
    """
    indices = list(merged_cluster)
    if len(indices) < 2:
        return float('inf')
    total_wpd_sum = 0.0
    for data_point1, data_point2 in combinations(indices, 2):
        total_wpd_sum += distance_matrix[data_point1][data_point2]
    n = len(indices)
    return (2 / (n * (n - 1))) * total_wpd_sum

def compute_graph_based_compactness(merged_cluster: Tuple, distance_matrix: List[List[float]], threshold: float) -> float:
    """
    Compute the graph-based compactness for a cluster.

    Parameters:
    - merged_cluster: Indices of points in the cluster.
    - distance_matrix: The precomputed distance matrix.
    - threshold: The current distance threshold.

    Returns:
    - Compactness of the cluster based on the average distance over edges in the semi-complete linkage graph.
    """
    indices = list(merged_cluster)
    if len(indices) < 2:
        return float('inf')

    # Calculate graph-based compactness
    total_wpd_sum = 0.0
    num_edges = 0
    for i, j in combinations(indices, 2):
        if distance_matrix[i][j] < threshold:
            total_wpd_sum += distance_matrix[i][j]
            num_edges += 1

    if num_edges == 0:
        return float(0)  # No edges in the semi-complete graph
    return (total_wpd_sum / num_edges)

def default_qoc(current_comp: float, min_dist_thresh: float, num_clusters: int, num_samples: int) -> float:
    """
    returns default QOC calculation: compactness / min_dist_thresh."""
    return current_comp / min_dist_thresh if min_dist_thresh else float('inf')

def weighted_qoc(current_comp: float, min_dist_thresh: float, num_clusters: int, num_samples: int) -> float:
    """
    Weighted QOC calculation: (compactness / min_dist_thresh) * num_clusters.
    This emphasizes the number of clusters at the current threshold.
    """
    return (current_comp / min_dist_thresh) * num_clusters if min_dist_thresh else float('inf')

def normalized_weighted_qoc(current_comp: float, min_dist_thresh: float, num_clusters: int, num_samples: int) -> float:
    """
    Normalized weighted QOC calculation: 
    (compactness / min_dist_thresh) * (num_clusters / num_samples).
    Keeps QOC values in a similar range as the default method.
    """
    return (current_comp / min_dist_thresh) * (num_clusters / num_samples) if min_dist_thresh else float('inf')


def graph_based_qoc(current_comp: float, min_dist_thresh: float, num_clusters_2 : float, num_samples : int) -> float:
    """
    Compute QOC as avgcomp / mindist for the given threshold.
    
    Parameters:

    Returns:
    - QOC value.
    """
    return current_comp / min_dist_thresh