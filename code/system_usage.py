from typing import List, Dict, Tuple, Callable 
import os
import doctest 
import doctest 
from multiprocessing import Pool
import logging 
import psutil

def log_system_usage(step: str) -> None:
    process = psutil.Process(os.getpid())
    memory = process.memory_info().rss / (1024 * 1024)  # Convert to MB
    cpu = process.cpu_percent(interval=0.1)  # Get CPU usage over a short interval
    logging.info(f"[{step}] Memory Usage: {memory:.2f} MB, CPU Usage: {cpu:.2f}%")

