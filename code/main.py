import doctest
import analysis;  
from bs4 import BeautifulSoup
from collections import Counter
from typing import List, Dict, Tuple, Callable 
import os
import doctest 
from sklearn.cluster import AgglomerativeClustering
from itertools import combinations
import doctest 
import matplotlib.pyplot as plt
import numpy as np
from multiprocessing import Pool
import concurrent.futures 
from datetime import datetime
from itertools import combinations
import random
import seaborn as sns
import logging 
import psutil
import system_usage, process_html, distance_matrix, qoc_vs_threshold, qoc, graphing
import qoc, distance_matrix
# from system_usage import log_system_usage
# from process_html import all_together
# from distance_matrix import compute_distance_matrix_unique
# from qoc_vs_threshold import find_threshold_table, analyze_labels
# from qoc import cluster_test
# from graphing import plot_from_sqlite_data
# from qoc import default_qoc, weighted_qoc, normalized_weighted_qoc
import argparse
# from distance_matrix import wpd, pd
import sqlite3
from HtmlTagNames import html_tag_names


def main(): 
    """
    code that has user interact with terminal to run the analysis and acquisition modules. 
    for analysis, user must input "python main.py analyze directory_name_of_HTML_files_to_analyze"
    for acquisition
    """

    default_tags = ["a", "abbr", "address", "area", "article", "aside", "audio", "b", "base", "bdi", "bdo", "blockquote", "br", "button", "canvas", "caption", "cite", "code", "col", "colgroup", "command", "datalist", "dd", "del", "details", "dfn", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6", "header", "hgroup", "hr", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li", "link", "map", "mark", "math", "menu", "meta", "meter", "nav", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "script", "section", "select", "small", "source", "span", "strong", "style", "sub", "summary", "sup", "svg", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "u", "ul", "var", "video", "wbr"]

    parser = argparse.ArgumentParser(description='Perform analysis of HTML files or graph')

    subparsers = parser.add_subparsers(dest='action', required=True, help='Action to perform')

    analyze_parser = subparsers.add_parser('analyze', help='Analyze HTML files in a directory')
    analyze_parser.add_argument('target', help='Directory name of HTML files to analyze')
    analyze_parser.add_argument('output_dir', default='output', help='Directory where results should be saved')
    analyze_parser.add_argument("--tags", nargs="+", help="List of tags to count", default=default_tags)
    analyze_parser.add_argument("--sample_distance", choices=["wpd", "pd"], default="wpd", help="Distance metric to use for the distance matrix (default: wpd)")
    analyze_parser.add_argument("--linkages", nargs="+", choices=["complete", "average", "single"], default=["complete", "average", "single"], help="Linkage types to use for clustering (default: ['complete', 'average', 'single]).")
    analyze_parser.add_argument("--qoc_function", choices=["default", "weighted", "normalized_weighted"], default="default", help="QOC function to use for quality of clustering calculation (default: 'default')"
)

    
    graph_parser = subparsers.add_parser('graph', help='Graph data from an SQLite file')
    graph_parser.add_argument('target', help='path to SQLite file to graph information from')
    graph_parser.add_argument('table_name', help='Name of the data table to graph')
    graph_parser.add_argument('output_dir', nargs='?', default='output', help='Directory where results should be saved')
    graph_parser.add_argument('--x_column', default="threshold", help="Column name for the x-axis. Defaults to 'threshold'.")
    graph_parser.add_argument('--y_column', default="qoc", help="Column name for the y-axis. Defaults to 'qoc'.")
    graph_parser.add_argument('--x_limits', type=str, help="Comma-separated range for x-axis (e.g., '0.1,0.5').")
    graph_parser.add_argument('--y_limits', type=str, help="Comma-separated range for y-axis (e.g., '10,50').")


    # parser.add_argument('action', choices=['analyze', 'graph'], help='Action to perform')
    # parser.add_argument('target', help='directory name of HTML files to analyze or sqlite file to graph information of')
    # parser.add_argument('output_dir', default='output', help='Directory where results should be saved')

    args = parser.parse_args()

    if not os.path.exists(args.target):
        raise ValueError(f"The provided target folder path (one we are reading from does not exist. Check {args.target}.")

    # if not os.path.isdir(args.target):
    #     raise ValueError(f"The provided target folder path (one we are reading from is not a directory. Check {args.target}")

    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)
    
    # Set the initial log filename
    log_directory = args.output_dir
    base_log_filename = 'dataclustering.log'
    log_file_path = os.path.join(log_directory, base_log_filename)
    
    # Dynamically create a unique log file name if it already exists
    i = 1
    while os.path.exists(log_file_path):
        log_file_path = os.path.join(log_directory, f'dataclustering_{i}.log')
        i += 1

    # Configure logging with dynamic log file path
    logging.basicConfig(
        filename=log_file_path,
        filemode='a',
        level=logging.INFO,
        format='%(levelname)s - %(asctime)s - %(message)s'
    )
    
    print(f"our logging file is at {log_file_path}")
    # logging.info(f"Created output directory: {args.output_dir}")
    logging.info(f"Log file path set to: {log_file_path}")
    logging.info(f"we have gotten through validating all user inputs. directories entered work.")

    if args.action == 'analyze':
        distance_function = {"wpd": distance_matrix.wpd, "pd": distance_matrix.pd}[args.sample_distance]
        qoc_function_map = {
            "default": qoc.default_qoc,
            "weighted": qoc.weighted_qoc,
            "normalized_weighted": qoc.normalized_weighted_qoc}
        qoc_function = qoc_function_map[args.qoc_function]

        html_files = {}
        for filename in os.listdir(args.target):
            if filename.endswith(".html"):
                file_path = os.path.join(args.target, filename)
                with open(file_path, "r", encoding="utf-8") as file:
                    html_files[filename] = file.read()

        # Call all_together to process the HTML files
        logging.info(f"Running analysis on directory: {args.target}")
        normalized_tags = [tag.lower() for tag in args.tags]
        invalid_tags = [tag for tag in normalized_tags if tag not in html_tag_names]

        if invalid_tags:
            raise ValueError(f"The following tags are invalid: {', '.join(invalid_tags)}")
        logging.info(f"Making a tag vector from the following tags: {normalized_tags}")
        # tags_of_interest : List[str] = ["a", "abbr", "address", "area", "article", "aside", "audio", "b", "base", "bdi", "bdo", "blockquote", "br", "button", "canvas", "caption", "cite", "code", "col", "colgroup", "command", "datalist", "dd", "del", "details", "dfn", "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "footer", "form", "h1", "h2", "h3", "h4", "h5", "h6", "header", "hgroup", "hr", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li", "link", "map", "mark", "math", "menu", "meta", "meter", "nav", "noscript", "object", "ol", "optgroup", "option", "output", "p", "param", "pre", "progress", "q", "rp", "rt", "ruby", "s", "samp", "script", "section", "select", "small", "source", "span", "strong", "style", "sub", "summary", "sup", "svg", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "time", "title", "tr", "track", "u", "ul", "var", "video", "wbr"]
        tag_vecs = process_html.all_together(html_files, normalized_tags)
        # print(tag_vecs)
        logging.info("we have found all tag vectors")
        matrix = distance_matrix.compute_distance_matrix_unique(tag_vecs, distance_metric=distance_function)
        logging.info("we've computed the distance matrix")
        for linkage in args.linkages:
            logging.info(f"we're using the {linkage} kind of linkage")
            better_threshold, results = qoc_vs_threshold.find_threshold_table(matrix, linkage, qoc_function)
            
            filename = os.path.basename(args.target)
            sample_distance = args.sample_distance
            qoc_function_name = args.qoc_function
            sqlite_file_path = os.path.join(args.output_dir, f"{filename}_{linkage}_{sample_distance}_{qoc_function_name}_clustering_results.sqlite")
            i = 1
            while os.path.exists(sqlite_file_path):
                sqlite_file_path = os.path.join(args.output_dir, f"{i}_{filename}_{linkage}_{sample_distance}_{qoc_function_name}_clustering_results.sqlite")
                i += 1

        # Save results to SQLite
            conn = sqlite3.connect(sqlite_file_path)
            cursor = conn.cursor()
            cursor.execute("""
                CREATE TABLE IF NOT EXISTS clustering_results (
                threshold REAL,
                qoc REAL,
                num_clusters INTEGER
                )
                """)
            cursor.executemany("INSERT INTO clustering_results (threshold, qoc, num_clusters) VALUES (?, ?, ?)", results)
            conn.commit()
            conn.close()

            logging.info(f"Clustering results for {linkage} saved to {sqlite_file_path}")
            print(f"The SQLite file path for {linkage} is {sqlite_file_path}")

            logging.info(f"we've found the best threshold for {linkage} type of linkage and it is {better_threshold}")
            system_usage.log_system_usage(f"we're finished finding best threshold for {linkage}")
            labels = qoc.cluster_test(matrix, better_threshold)
            logging.info(f"the labels for this best threshold look like {labels}")
            system_usage.log_system_usage(f"we finished finding labels for {linkage}")
            num_clust = qoc_vs_threshold.analyze_labels(labels) + 1 
            logging.info(f"for {linkage} type of linkage the number of clusters when using the best threshold is {num_clust}")
            logging.info("Analysis completed successfully")

    if args.action == 'graph':
        logging.info("We are now graphing")

        x_column = getattr(args, "x_column", "threshold")
        y_column = getattr(args, "y_column", "qoc")
        base_name = os.path.splitext(os.path.basename(args.target))[0]

        # print(f"Raw x_limits from args: {args.x_limits}")
        # print(f"Raw y_limits from args: {args.y_limits}")

        def parse_limits(limit_str):
            if limit_str:
                try:
                    limits = tuple(map(float, limit_str.split(',')))
                    if len(limits) != 2:
                        raise ValueError
                    return limits
                except ValueError:
                    raise ValueError(f"Invalid limits format: {limit_str}. Use 'min,max'.")
            return None
        
        x_limits = parse_limits(args.x_limits)
        y_limits = parse_limits(args.y_limits)

        conn = sqlite3.connect(args.target)
        cursor = conn.cursor()
        try:
            cursor.execute(f"SELECT name FROM sqlite_master WHERE type='table' AND name='{args.table_name}'")
            if not cursor.fetchone():
                raise ValueError(f"Table '{args.table_name}' does not exist in the database.")

            cursor.execute(f"PRAGMA table_info({args.table_name})")
            columns = [col[1] for col in cursor.fetchall()]
            if x_column not in columns or y_column not in columns:
                raise ValueError(f"Specified columns '{x_column}' and/or '{y_column}' do not exist in the table.")

            cursor.execute(f"SELECT {x_column}, {y_column} FROM {args.table_name}")
            data = cursor.fetchall()
            if not data:
                raise ValueError(f"No data found in the table '{args.table_name}' for columns '{x_column}' and '{y_column}'.")
        except Exception as ex:
            logging.error(f"Error fetching data: {ex}")
            raise
        finally:
            conn.close()

        title = f"{base_name}: {y_column} versus {x_column}"
        fig = graphing.plot_from_sqlite_data(data, x_column, y_column, title=title, x_limits=x_limits, y_limits=y_limits)

        if args.output_dir:
            if os.path.isdir(args.output_dir):
                output_path = os.path.join(args.output_dir, f"{base_name}_graph.png")
                count = 1
                while os.path.exists(output_path):
                    output_path = os.path.join(args.output_dir, f"{count}_{base_name}_graph.png")
                    count += 1
            else:
                output_path = args.output_dir  # Assuming it's a full path with file name
            fig.savefig(output_path)
            logging.info(f"Graph saved to {output_path}")
        else:
            logging.info("No output path provided. Showing the graph.")
            plt.show()




if __name__ == "__main__":
    print("Running doctests for all modules...")
    # doctest.testmod(process_html)
    # doctest.testmod(distance_matrix)
    doctest.testmod(qoc)
    # doctest.testmod(qoc_vs_threshold)



    # print("Doctests completed. Running main program...")
    main()
