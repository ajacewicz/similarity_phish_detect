from typing import List, Tuple, Dict, Callable
import os
import doctest 
import doctest 
from multiprocessing import Pool
import random
import logging


def weighted_dif(list1: List[int], list2 : List[int]) -> float:
    """""
    >>> weighted_dif([1, 2, 3], [4, 5, 6])
    1.85
    >>> weighted_dif([0], [0])
    0.0
    >>> weighted_dif([1,2], [1,2])
    0.0
    >>> weighted_dif([1,1,1], [0,0,0])
    3.0
    >>> weighted_dif([1,0,1], [0,0,0])
    2.0
    >>> weighted_dif([1, 2, 3], [4, 5])  # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    ValueError: Input lists must have the same length.

    Calculate the weighted difference as defined in Cui's thesis. 

    Parameters:
    list1 (list of int): First list of integers. Here, it is the list of
    ints representing the number of times each HTML tag from our corpus is
    found in a specific webpage. 

    list2 (list of int): Second list of integers. Here, it is the list of
    ints representing the number of times each HTML tag from our corpus is
    found in a specific webpage. 

    Returns:
    float: The calculated weighted difference between the two lists.
    """

    # check to ensure lists are of the same length 
    # if they aren't, there is something wrong with how they were formed
    # as every list here have a length equal to the number of elements
    # in the tags_of_interest list
    if len(list1) != len(list2):
        raise ValueError("Input lists must have the same length.")

    # Initialize the total score
    total_score : float = 0.0

    # Iterate through the lists one at a time
    for num1, num2 in zip(list1, list2):
        # Calculate the absolute difference between the numbers
        absolute_diff : float = abs(num1 - num2)
        
        # Calculate the maximum value between the two numbers
        max_value = max(num1, num2)
        
        # Avoid division by zero (doing this is okay bc if max_value is zero
        # then this implies that values at list[indice] is 0 for both list1 and
        # list 2 and so the difference is equal to 0)
        if max_value == 0:
            continue
        
        # Calculate the score for the current index
        score : float = absolute_diff / max_value
        
        # Add the score to the total score
        total_score += score

    return total_score


def EQU(num1: int, num2: int) -> int:
    """
    >>> EQU(0, 0)
    0
    >>> EQU(0, 1)
    0
    >>> EQU(1, 0)
    0
    >>> EQU(1, 1)
    1
    >>> EQU(2, 2)
    1
    >>> EQU(-1, -1)
    1
    >>> EQU(-1, 1)
    0
    >>> EQU(1, 2)
    0

    Check if two integers are equal and not equal to 0.

    Parameters:
    num1 (int): First integer.
    num2 (int): Second integer.

    Returns:
    int: 1 if the integers are equal to each other and not equal to 0,
    returns 0 otherwise.
    """
    if num1 == num2 and num1 != 0:
        return 1
    else:
        return 0

def S(list1: List[int], list2: List[int]) -> int:
    """
    >>> S([1, 2, 3], [1, 2, 3])
    3
    >>> S([1, 1, 1], [0, 0, 0])
    0
    >>> S([0, 0, 0], [1, 1, 1])
    0
    >>> S([1, 1, 1], [1, 1, 1])
    3
    >>> S([0], [0])
    0
    >>> S([1, 2, 0, 0, 12, 15, 16], [1, 2, 0, 0, 1, 2, 35])
    2

    Sum the results of running EQU on two lists of integers.

    Parameters:
    list1 (List[int]): First list of integers.
    list2 (List[int]): Second list of integers.

    Returns:
    int: Sum of the results of running EQU.
    """  
    total: int = 0
    for num1, num2 in zip(list1, list2):
        total += EQU(num1, num2)
    return total

def wpd (list1: List[int], list2: List[int]) -> float:
   """
    Calculate the weighted proportion difference as defined in Cui's paper.

    Parameters:
    list1 (List[int]): First list of integers.
    list2 (List[int]): Second list of integers.

    Returns:
    float: The weighted proportion difference between list1 and list2.

    >>> wpd([0], [0])
    0.0
    >>> wpd([1,2], [1,2])
    0.0
    >>> wpd([1, 2, 3], [1, 2, 3])
    0.0
    >>> wpd([1, 1, 1], [0, 0, 0])
    1.0
    >>> wpd([0, 0, 0], [1, 1, 1])
    1.0
    >>> wpd([1, 1, 1], [1, 1, 1])
    0.0
    >>> wpd([1, 2, 0, 0, 12, 15, 16], [1, 2, 0, 0, 1, 2, 35])
    0.5376995046780407
    >>> wpd([1, 2, 3], [4, 5, 6])
    1.0
   """
  # calculate the weighted difference 
   wd : float = weighted_dif(list1, list2)

  # calcuate the sum that is included in the denominator 
   s_val : int = S(list1, list2)

  # calculate the denominator 
   denominator : float = wd + s_val 

  # Avoid division by zero
  # I think this is a reasonable thing to do here, not sure what the alt would be
   if denominator == 0:
      return 0.0

   wpd_value: float = wd / denominator

   return wpd_value 

def D(list1: List[int], list2 : List[int]) -> float:
    """
    >>> D([0, 0, 0], [1, 1, 1])
    3
    >>> D([1, 1, 1], [1, 1, 1])
    0
    >>> D([1, 2, 3], [1, 1, 1])
    2
    >>> D([0, 0, 0, 4], [1, 1, 1])
    Traceback (most recent call last):
    ...
    ValueError: Lists should have the same length
    """
    if len(list1) != len(list2):
        raise ValueError("Lists should have the same length")

    return sum(1 if list1[i] != list2[i] else 0 for i in range(len(list1))) 

def L(list1: List[int], list2 : List[int]) -> float:
    """
    >>> L([0, 0, 0], [1, 1, 1])
    3
    >>> L([1, 1, 1], [1, 1, 1])
    3
    >>> L([1, 2, 3], [1, 1, 1])
    3
    >>> L([0, 0, 0], [0, 0, 0])
    0
    >>> L([0, 1, 0], [0, 0, 0])
    1
    >>> L([0, 0, 0, 4], [1, 1, 1])
    Traceback (most recent call last):
    ...
    ValueError: Lists should have the same length
    """
    if len(list1) != len(list2):
        raise ValueError("Lists should have the same length")
    return sum(1 if list1[i]!= 0 or list2[i]!=0 else 0 for i in range(len(list1)))

def pd (list1: List[int], list2: List[int]) -> float:
    """
    >>> pd([1, 2, 3, 4, 5, 0, 0, 0, 0], [1, 0, 0, 0, 0, 6, 7, 0, 0])
    0.8571428571428571
    >>> pd([0, 0, 0, 4], [1, 1, 1])
    Traceback (most recent call last):
    ...
    ValueError: Lists should have the same length
    """
    d = D(list1, list2)
    l = L(list1, list2)
    pds = d/l
    return pds 

def compute_distance_matrix_unique(unique_counts: List[Tuple[int]], batch_size: int = 1, distance_metric: Callable[[List[int], List[int]], float] = wpd) -> List[List[float]]:
    """
    Computes the distance matrix using wpd for unique tag vectors.

    Parameters:
    - unique_counts (List[Tuple[int]]): List of unique tag vectors.
    - batch_size (int): Number of pairs of distances to compute per batch.
    - distance_metric (Callable): The distance function to use (e.g., `wpd` or `pd`).


    Returns:
    - List[List[float]]: A matrix representing the distances between unique tag vectors.

    >>> compute_distance_matrix_unique([(10, 20), (15, 25)])
    [[0.0, 1.0], [1.0, 0.0]]
    >>> compute_distance_matrix_unique([(15, 25), (10, 20)])
    [[0.0, 1.0], [1.0, 0.0]]
    >>> compute_distance_matrix_unique([(6, 10, 20, 100), (6, 10, 20, 100)])
    Traceback (most recent call last):
    ...
    AssertionError: num_unique is not equal to the number of unique counts. bad input
    >>> compute_distance_matrix_unique([(1, 3, 30, 13), (6, 10, 20, 100), (11, 16, 2, 50)])
    [[0.0, 1.0, 1.0], [1.0, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(1, 3, 30, 13), (1, 10, 20, 100), (11, 16, 2, 50)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(11, 16, 2, 50), (1, 3, 30, 13), (1, 10, 20, 100)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(1, 3, 30, 13), (1, 10, 20, 100), (1, 3, 30, 13)])
    Traceback (most recent call last):
    ...
    AssertionError: num_unique is not equal to the number of unique counts. bad input
    >>> compute_distance_matrix_unique([(11, 16, 2, 50), (1, 3, 30, 13), (1, 10, 20, 100)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(11, 16, 2, 50), (1, 10, 20, 100), (1, 3, 30, 13)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    >>> compute_distance_matrix_unique([(1, 10, 20, 100), (1, 3, 30, 13), (11, 16, 2, 50)])
    [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
    """
    num_unique = len(unique_counts)
    sorted_unique_counts = sorted(unique_counts)
    # print(sorted_unique_counts)
    assert num_unique == len(set(unique_counts)), "num_unique is not equal to the number of unique counts. bad input"

    # Initialize the distance matrix for unique vectors
    distance_matrix: List[List[float]] = [[0.0] * num_unique for _ in range(num_unique)]
    
    batches = [(i, j) for i in range(num_unique) for j in range(i + 1, num_unique)]

    with Pool(processes=os.cpu_count()) as pool:
        #distance_metric
        args = [(sorted_unique_counts, batches[i:i + batch_size], distance_metric) for i in range(0, len(batches), batch_size)]
        
        results = pool.map(compute_batch_distances_unique, args)

        for batch_results in results:
            for i, j, distance in batch_results:
                distance_matrix[i][j] = distance
                distance_matrix[j][i] = distance 

    logging.info(f"the distance matrix when using {distance_metric.__name__} looks like {distance_matrix}")

    # print(distance_matrix)
    return distance_matrix

# Callable[[List[int], List[int]], float]]
def compute_batch_distances_unique(args: Tuple[List[Tuple[int]], List[Tuple[int, int]], Callable[[List[int], List[int]], float]]) -> List[Tuple[int, int, float]]:
    """
    Computes distances for a batch of unique vector pairs.

    Parameters:
    - args (Tuple[List[Tuple[int]], List[Tuple[int, int]]]): A tuple containing the list of unique tag vectors,
    a batch of vector index pairs and the distance function to use.

    Returns:
    - List[Tuple[int, int, float]]: A list of tuples containing indices and their computed distances.
    """
    # distance_fun
    unique_counts, batch, distance_matrix = args
    results = []
    for i, j in batch:
        distance = distance_matrix(unique_counts[i], unique_counts[j])  
        results.append((i, j, distance))
    return results

def compute_test_distance_matrix(test_tag_vec : List[Tuple[int]], train_tag_vec: List[Tuple[int]], batch_size: int=1, distance_metric: Callable[[List[int], List[int]], float] = wpd) -> List[List[float]]:
    """Computes the distance matrix between test tag vectors and training tag vectors
    The distance matrix returned will have rows corresponding to test samples, and columns corresponding to training samples"""
    num_test = len(test_tag_vec)
    num_train = len(train_tag_vec)
    test_distance_matrix: List[List[float]] = [[0.0] * num_train for _ in range(num_test)]
    batches = [(i,j) for i in range(num_test) for j in range(num_train)]
    with Pool(processes=os.cpu_count()) as pool:
        args = [(test_tag_vec, train_tag_vec, batches[i:i + batch_size], distance_metric)
                for i in range(0, len(batches), batch_size)]
        results = pool.map(compute_batch_test_distances, args)

        for batch_results in results:
            for i, j, distance in batch_results:
                test_distance_matrix[i][j] = distance
    logging.info(f"Computed test distance matrix: {test_distance_matrix}")
    return test_distance_matrix

def compute_batch_test_distances(args : Tuple[List[Tuple[int]], List[Tuple[int]], List[Tuple[int, int]], Callable[[List[int]], float]]) -> List[Tuple[int,int,float]]:
    """Compute distance btw test and training samples in batches"""
    test_vec, train_vec, batch, distance_metric = args
    results = []
    for i,j in batch:
        distance = distance_metric(test_vec[i], train_vec[j])
        results.append((i,j,distance))
    return results 


def random_permute_distance_matrix(distance_matrix: list[list[float]]) -> list[list[float]]:
    indices = list(range(len(distance_matrix)))
    random.shuffle(indices)  
    # print(indices)
    
    permuted_matrix = [[distance_matrix[i][j] for j in indices] for i in indices]
    
    return permuted_matrix


# def compute_distance_matrix(data_dict: Dict[str, List[int]]) -> List[List[float]]:
#     """
#     Computes the distance matrix using wpd for every combination of keys in the
#     dictionary that contains each list of ints that represents the number of HTML
#     tags in a given webpage (keeping in mind that the distance between 
#     (a and b) and (b and a) is the same thing, so we only need one of them to know
#     both. 
      
#     Parameters:
#     - data_dict (Dict[str, List[int]]): The dictionary that contains all lists of
#       ints that represents the number of HTML tags in a given webpage. 

#     Returns:
#     - List[List[float]]: A matrix representing the distances between HTML tag
#     occurances, with distance being defined as weighted proportional distance (wpd).

#     >>> compute_distance_matrix({'file1': [10, 20], 'file2': [15, 25]})
#     [[0.0, 1.0], [1.0, 0.0]]
#     >>> compute_distance_matrix({'file2': [15, 25], 'file1': [10, 20]})
#     [[0.0, 1.0], [1.0, 0.0]]
#     >>> compute_distance_matrix({'file1': [6, 10, 20, 100], 'file2': [6, 10, 20, 100]})
#     [[0.0, 0.0], [0.0, 0.0]]
#     >>> compute_distance_matrix({'file2': [6, 10, 20, 100], 'file1': [6, 10, 20, 100]})
#     [[0.0, 0.0], [0.0, 0.0]]
#     >>> compute_distance_matrix({'file1': [1, 3, 30, 13], 'file2': [6, 10, 20, 100], 'file3': [11, 16, 2, 50]})
#     [[0.0, 1.0, 1.0], [1.0, 0.0, 1.0], [1.0, 1.0, 0.0]]
#     >>> compute_distance_matrix({'file1': [1, 3, 30, 13], 'file2': [1, 10, 20, 100], 'file3': [11, 16, 2, 50]})
#     [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
#     >>> compute_distance_matrix({'file3': [11, 16, 2, 50], 'file1': [1, 3, 30, 13], 'file2': [1, 10, 20, 100]})
#     [[0.0, 0.6555683122847302, 1.0], [0.6555683122847302, 0.0, 1.0], [1.0, 1.0, 0.0]]
#     """
#     keys = sorted(data_dict.keys())  
#     #keys = list(data_dict.keys())
#     num_keys = len(keys)
    
#     distance_matrix = [[0.0] * num_keys for _ in range(num_keys)]
    
#     for i in range(num_keys):
#         key1 = keys[i]
#         for j in range(i + 1, num_keys):  # Start from i + 1 to avoid computing distances twice
#             key2 = keys[j]

#             # Compute the distance for the pair of keys
#             distance = wpd(data_dict[key1], data_dict[key2])

#             # Store the computed distance in the distance matrix
#             distance_matrix[i][j] = distance_matrix[j][i] = distance

#     assert is_matrix_sorted(data_dict, distance_matrix), "distance matrix isn't sorted"

#     return distance_matrix