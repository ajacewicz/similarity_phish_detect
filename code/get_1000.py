import sqlite3
import csv

connection = sqlite3.connect('data-base-name.db') 
cursor = connection.cursor()

query = """
SELECT content_hash
FROM http_responses
WHERE content_hash IS NOT NULL
ORDER BY RANDOM()
LIMIT 1000;
"""

cursor.execute(query)
rows = cursor.fetchall()

with open('output.csv', 'w', newline='', encoding='utf-8') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerows(rows) 

connection.close()
