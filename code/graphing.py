import os
import matplotlib.pyplot as plt
import logging 
from qoc_vs_threshold import find_threshold_table
import sqlite3


def plot_from_sqlite_data(data, x_column: str = "threshold", y_column: str = "qoc", title: str = None, x_limits: tuple = None, y_limits: tuple = None):
    """
    Generates a graph from provided data.

    Args:
        data (list of tuples): List of (x, y) data to plot.
        x_column (str): Label for the x-axis. Defaults to "threshold".
        y_column (str): Label for the y-axis. Defaults to "qoc".
        title (str): Title of the graph. If None, a default title is generated.
        x_limits (tuple): Tuple specifying the (min, max) range for the x-axis. If None, defaults are used.
        y_limits (tuple): Tuple specifying the (min, max) range for the y-axis. If None, defaults are used.


    Returns:
        matplotlib.figure.Figure: The generated graph figure.
    """
    # print(f"x_limits looks like {x_limits}")
    # print(f"y_limits looks like {y_limits}")

    if not data:
        raise ValueError("No data provided for plotting.")

    # Extract x and y data
    x_data, y_data = zip(*data)

    # Generate default title if not provided
    if not title:
        title = f"{y_column} vs {x_column}"

    # Create the graph
    fig, ax = plt.subplots(figsize=(8, 6))
    ax.plot(x_data, y_data, marker='o', linestyle='-', color='b')
    ax.set_xlabel(x_column)
    ax.set_ylabel(y_column)
    ax.set_title(title)
    ax.grid(True)

    if x_limits:
        ax.set_xlim(x_limits)
    if y_limits:
        ax.set_ylim(y_limits)

    return fig

# def plot_from_sqlite(db_path: str, table_name: str, output_path: str = None, x_column: str = "threshold", y_column: str = "qoc", title : str =  "Threshold versus QOC values",):
#     """
#     Reads data from a SQLite table and creates a graph.
    
#     Args:
#         db_path (str): name of the SQLite database file.
#         table_name (str): Name of the table to query.
#         x_column (str): Column name for the x-axis.
#         y_column (str): Column name for the y-axis.
#         output_path (str, optional): File path to save the graph. If None, the graph is displayed.
        
#     Raises:
#         ValueError: If the table or columns are not found in the database.
#     """
#     # Connect to the SQLite database
#     logging.info("we have entered the graphing module")

#     conn = sqlite3.connect(db_path)
#     cursor = conn.cursor()
#     logging.info("we have connected")

#     try:
#         # Check if the table exists
#         cursor.execute(f"SELECT name FROM sqlite_master WHERE type='table' AND name='{table_name}'")
#         if not cursor.fetchone():
#             logging.error(f"Table '{table_name}' does not exist in the database.")
#             raise ValueError(f"Table '{table_name}' does not exist in the database.")

#         # Check if the columns exist
#         cursor.execute(f"PRAGMA table_info({table_name})")
#         columns = [col[1] for col in cursor.fetchall()]
#         if x_column not in columns or y_column not in columns:
#             logging.error(f"One or both columns '{x_column}', '{y_column}' do not exist in the table '{table_name}'.")
#             raise ValueError(f"One or both columns '{x_column}', '{y_column}' do not exist in the table '{table_name}'.")

#         # Query the data
#         cursor.execute(f"SELECT {x_column}, {y_column} FROM {table_name}")
#         data = cursor.fetchall()

#         if not data:
#             logging.error(f"No data found in the table '{table_name}' for columns '{x_column}' and '{y_column}'.")
#             raise ValueError(f"No data found in the table '{table_name}' for columns '{x_column}' and '{y_column}'.")

#         # Extract x and y data
#         x_data, y_data = zip(*data)

#         # Create the graph
#         plt.figure(figsize=(8, 6))
#         plt.plot(x_data, y_data, marker='o', linestyle='-', color='b')
#         plt.xlabel(x_column)
#         plt.ylabel(y_column)
#         plt.title(title)
#         # plt.title(f"{y_column} vs {x_column}")
#         plt.grid(True)

#         if output_path:
#             if os.path.isdir(output_path):
#                 output_path = os.path.join(output_path, "graph.png")        
#             plt.savefig(output_path)
#             logging.info(f"Graph saved to {output_path}")
#         else:
#             logging.info(f"no output path given, so just showing graph")
#             plt.show()
#     except sqlite3.DatabaseError as db_err:
#         logging.error(f"Database error: {db_err}")
#         raise
#     except ValueError as val_err:
#         logging.error(f"Value error: {val_err}")
#         raise
#     except Exception as ex:
#         logging.error(f"An unexpected error occurred: {ex}")
#         raise
#     finally:
#     # Close the database connection
#         conn.close()
    
