import os
import doctest 
import doctest 
from multiprocessing import Pool
import random
import shutil
from sklearn.cluster import AgglomerativeClustering
from typing import Dict, List, Tuple
from process_html import all_together, process_html_files_p, transform_tag_counts_to_lists_p
from distance_matrix import compute_distance_matrix_unique, compute_test_distance_matrix



# Believe this is working, want to reclone the 1k html file directory and make sure it divides correctly
def split_data(directory, train_dir, test_dir, train_ratio=0.8, seed=42):
    random.seed(seed)
    files = os.listdir(directory)
    print(f"Original files in directory: {files}")
    random.shuffle(files)
    train_size = int(len(files) * train_ratio)
    train_files = files[:train_size]
    print(train_files)
    test_files = files[train_size:]
    print(test_files)
    os.makedirs(train_dir, exist_ok=True)
    os.makedirs(test_dir, exist_ok=True)
    for file in train_files:
        shutil.copy(os.path.join(directory, file), os.path.join(train_dir, file))
    for file in test_files:
        shutil.copy(os.path.join(directory, file), os.path.join(test_dir, file))


def check_no_overlap(dir1, dir2):
    # Get sets of filenames in each directory
    files1 = set(os.listdir(dir1))
    files2 = set(os.listdir(dir2))
    
    # Find intersection (common files)
    common_files = files1 & files2
    
    if common_files:
        print(f"Overlap found! Common files: {common_files}")
        return False
    else:
        print("No overlap. The directories contain only different files.")
        return True

def process_training_testing_data(train_dir, test_dir, tags_of_interest, distance_metric, batch_size : int = 1) -> Tuple[List[List[float]], List[Tuple[int]], List[List[float]], Dict[str, List[int]]]:
    """Process training and testing HTML files, getting distance matrix for training data and test-to-training data distance matrix as well"""
    train_tag_vec = all_together(train_dir, tags_of_interest)
    train_distance_matrix = compute_distance_matrix_unique(train_tag_vec, batch_size=batch_size, distance_metric=distance_metric)
    test_tag_vec = all_together(test_dir, tags_of_interest)
    test_distance_matrix = compute_test_distance_matrix(test_tag_vec, train_tag_vec, batch_size=batch_size, distance_metric=distance_metric)
    return train_distance_matrix, test_distance_matrix 


def classify_phishing(best_threshold, train_distance_matrix, test_distance_matrix, test_html_files, linkage="single", manual_clusters: dict[str, set[str]] | None = None) -> dict[str, str]:
    """Classifies test data as being phishing or not phishing based on same linkage used for the
    clustering of the training data we are comparing to. 

    If 'manual_clusters' are input then will treat them as being predefined instead of computing clusters. 

    steps here are 
    1. compute clustering using the given distance matrix and best threshold
    2. store clusters 
    3. process test data so that it has a distance matrix using correct distance value to do so -> if manual_clusters are input
    then this is where we start
    4. classify test data based on if its distance to any phishing cluster is less than best_threshold value"""
    if manual_clusters is not None:
        clusters = manual_clusters
    else:
        clustering = AgglomerativeClustering(n_clusters=None, metric="precomputed", linkage=linkage, distance_threshold=best_threshold)
        cluster_labels = clustering.fit_predict(train_distance_matrix)

        clusters = {}
        for idx, label in enumerate(cluster_labels):
            if label not in clusters:
                clusters[label] = set()
            clusters[label].add(idx)
    
    predictions = {}

    test_files = list(test_html_files.keys())
    for test_idx, test_file in enumerate(test_files):
        test_distances = test_distance_matrix[test_idx]

        if linkage == "single":
            min_dis_to_cluster = min(min(test_distances[i] for i in cluster) for cluster in clusters.value())
            is_phishing = min_dis_to_cluster <= best_threshold

        # for complete linkage will be very similar but will be based on max distance instead 

        if linkage == "complete":
            max_dis_to_cluster = min(max(test_distances[i] for i in cluster) for cluster in clusters.values())
            is_phishing = max_dis_to_cluster <= best_threshold

        if linkage == "average":
            min_avg_dist_to_cluster = min(sum(test_distances[i] for i in cluster) / len(cluster) for cluster in clusters.values())
            is_phishing = min_avg_dist_to_cluster <= best_threshold

        predictions[test_file] = "phishing" if is_phishing else "not phishing"
        print(predictions)
    return predictions
    
