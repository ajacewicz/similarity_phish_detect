from typing import List, Tuple, Callable
import os
import doctest 
from sklearn.cluster import AgglomerativeClustering
import doctest 
import matplotlib.pyplot as plt
from multiprocessing import Pool
import logging 
from system_usage import log_system_usage
from sklearn.cluster import AgglomerativeClustering
from qoc import compute_cluster_compactness, default_qoc, weighted_qoc, normalized_weighted_qoc, compute_graph_based_compactness, graph_based_qoc
import sqlite3
import numpy as np
from sklearn.metrics import silhouette_score


def find_threshold_table(wmp_matrix: List[List[float]], linkage_type, qoc_function: Callable[[float, float, int, int], float] = None) -> Tuple[float, List[Tuple[float, float, int]]]:
    """"
    Calculate the best qoc value possible and return a tuple of thresholds, qoc values and number of clusters
    for each distinct threshold reachable based on our data, as a data table in sqlite. 

    Parameters:
    - wmp_matrix: The distance matrix.
    - linkage_type: Linkage type for clustering (e.g., 'average', 'complete').
    - qoc_function: A callable (meaning function) defining how QOC is calculated.

    Returns:
    - best_threshold: The threshold with the minimum QOC.
    - results: A list of tuples (threshold, qoc, num_clusters). This is used to make our data table in main.py
    """
    clustering = AgglomerativeClustering(
        n_clusters=None,
        linkage=linkage_type,
        compute_full_tree=True,
        distance_threshold=0,
        metric='precomputed'
    )
    logging.info("we're about to fit")
    clustering.fit(wmp_matrix)
    logging.info("we've fit our model based on our distance matrix")

    # print(wmp_matrix)
    children = clustering.children_
    size_of_children = len(children)
    # print("Size of children:", size_of_children)
    logging.info("we've found the children attribute")

    distances = clustering.distances_
    logging.info("we've found the distances attribute")

    # print(children)
    
    labels = list(range(len(wmp_matrix)))  
    # min_dist = min_smallest_wpd_across_clusters(labels, wmp_matrix)
    # logging.info("we've found the minimum smallest wpd across all clusters")

    
    cluster_mapping = {i: (i,) for i in range(len(wmp_matrix))}
    compactness_memo = {i: float('inf') for i in range(len(wmp_matrix))}
    min_qoc = float('inf')
    best_qoc = float('-inf') if qoc_function == silhouette_score else float('inf')
    best_threshold = None
    results = []
    num_samples = len(wmp_matrix)
    # qoc_values = []  
    # threshold_values = []  
    # num_clusters_values = []
                
    log_system_usage(f"about to find qoc for all thresholds, we are using {qoc_function}")

    for i in range(clustering.children_.shape[0]):

        child1, child2 = children[i]

        new_cluster_id = len(wmp_matrix) + i  
        
        # if child1 < len(wmp_matrix) and child2 < len(wmp_matrix):
        #     merged_cluster = (child1, child2)
        #     new_label = new_cluster_id
        # else:
        #     merged_cluster = (cluster_mapping[child1] if child1 >= len(wmp_matrix) else (child1, )) + \
        #     (cluster_mapping[child2] if child2 >= len(wmp_matrix) else (child2,)) 
        #     new_label = new_cluster_id

        if child1 < len(wmp_matrix) and child2 < len(wmp_matrix):
            merged_cluster = (child1, child2)
            new_label = new_cluster_id
        else:
            merged_cluster = (cluster_mapping[child1] + cluster_mapping[child2])
            new_label = new_cluster_id

        for idx in merged_cluster:
            labels[idx] = new_label 
        
        cluster_mapping[new_cluster_id] = merged_cluster
        
        compactness_memo.pop(child1, None)
        compactness_memo.pop(child2, None)


        if child1 in cluster_mapping:
            del cluster_mapping[child1]
        if child2 in cluster_mapping:
            del cluster_mapping[child2]
        
        if qoc_function == graph_based_qoc:
            compactness_memo[new_cluster_id] = compute_graph_based_compactness(merged_cluster, wmp_matrix, distances[i])
            # print(compactness_memo)
        else:    
            compactness_memo[new_cluster_id] = compute_cluster_compactness(merged_cluster, wmp_matrix)


        if qoc_function != silhouette_score:
            # print("entered here")
            total_compactness = sum(compactness for cluster_id, compactness in compactness_memo.items() if len(cluster_mapping[cluster_id]) >= 2)
            # print(f"total compactness currently is {total_compactness}")
            num_clusters_2 = sum(1 for cluster_id in compactness_memo if len(cluster_mapping[cluster_id]) >= 2)
            # print(num_clusters_2)
            total_clusters = sum(1 for cluster_id in compactness_memo)
            if num_clusters_2 == 0:
                current_comp = float('inf')
            else:
                current_comp = total_compactness / num_clusters_2
                # print(f"current comp is currently {current_comp} at {num_clusters_2} number of clusters")
            # print(f"Merging clusters {merged_cluster} into {new_cluster_id}")
            # print(f"Current labels: {labels}")
            # print(f"Current cluster_mapping state: {cluster_mapping}") 
            debug_messages = []

            # print(labels)
            unique_labels = set(labels)
            if len(unique_labels) > 1:
                current_distance = distances[i]
                # print(current_distance)
                if i == len(distances) - 1 or distances[i + 1] > current_distance:
                    # 99% sure about this, but might need to be changed
                    min_dist_thresh = distances[i+1] if i+1 < len(distances) else None
                    print(f"min dist threshold currently is {min_dist_thresh}")
                    logging.debug(f"about to find qoc for comp value of {current_comp}")
                    qoc = qoc_function(current_comp, min_dist_thresh, num_clusters_2, num_samples)
                    print(f"qoc currently is {qoc}")
                    results.append((min_dist_thresh, qoc, total_clusters))
                    logging.debug(f"found qoc and it is {qoc}")
                    # qoc_values.append(qoc)
                    # threshold_values.append(min_dist_thresh)
                    # num_clusters_values.append(total_clusters)
                    # print("fcomp here is")
                    # print(fcomp)
                    # threshold = distances[i + 1] if i + 1 < len(distances) else None
                    # if qoc > 1:
                    #     print(f"qoc is greater than 1 at {min_dist_thresh}")
                    #     print(f"the comp value here is {fcomp} and qoc here is {qoc}")
                    # print(f"Minimum distance across clusters at distance {current_distance}: {min_dist}")
                    # print(f"Quality of Clustering (qoc) at threshold {threshold}: {qoc}")
                    if qoc < min_qoc:
                        min_qoc = qoc
                        best_threshold = min_dist_thresh
                else:
                    # print(f"Continuing to merge at distance {current_distance}; skipping qoc calculation.")
                    debug_messages.append(f"Skipped qoc calculation at distance {current_distance}")
            else:
                # print("All points are in a single cluster; skipping min_dist calculation.")
                debug_messages.append("All points are in a single cluster; skipping min_dist calculation.")
        elif qoc_function == silhouette_score:
            print("in here")
            best_silhouette_score = -1 
            label_array = np.array(labels)
            unique_labels = len(set(label_array))
            if unique_labels < 2:
                logging.warning(f"Skipping Silhouette Coefficient calculation: only {unique_labels} unique cluster(s) found.")
                continue
            silhouette_score_value = silhouette_score(wmp_matrix, label_array, metric="precomputed")
            results.append((distances[i], silhouette_score_value, unique_labels))

# need to make sure calling this a threahold is still accurate - believe so
            if silhouette_score_value > best_silhouette_score:
                    best_qoc = silhouette_score_value
                    best_threshold = distances[i+1]
                    logging.info(f"New best Silhouette Score found: {best_qoc} at distance {best_threshold}, after {i} merges")

    print(f"best threshold is {best_threshold}")
    return best_threshold, results


def analyze_labels(numbers: List[int]) -> Tuple[int, float]:
    if not numbers:
        raise ValueError("The list of numbers cannot be empty.")
    return max(numbers)